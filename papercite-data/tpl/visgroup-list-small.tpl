    @{group@
     @{entry@ <div id="paperkey_@papercite_id@" class="papercite_entry">[@key@] 
        @?pdf@ <a href="@PAPERCITE_DATA_URL@/@pdf@" title='Download PDF' class='papercite_pdf'><img src='@WP_PLUGIN_URL@/papercite/img/pdf.png' alt="[PDF]"/></a>@;pdf@
        @?doi@<a href='http://dx.doi.org/@doi@' class='papercite_doi' title='View document on publisher site'><img src='@WP_PLUGIN_URL@/papercite/img/external.png' width='10' height='10' alt='[DOI]' /></a>@;doi@
        @?vid@ <a href="@PAPERCITE_DATA_URL@/@vid@" title='Download Video' class='papercite_pdf'><img src='@PAPERCITE_DATA_URL@/images/video.png' height="16px" alt="[VID]"/></a>@;vid@
        @?youtube@ <a href="@youtube@" title='Download Video' class='papercite_pdf'><img src='@PAPERCITE_DATA_URL@/images/youtube.png' height="16px" alt="[YT]"/></a>@;youtube@
        <a href="/publications/@cite@">
        @#entry@
        </a>
    	<br/>
     @}entry@
    @}group@
