<div class="entry-content pad group">
<article>
<div class="entry themeform">
    <style>
    .pub_row {   
    display:float;	
    padding: 10px;


}

.pub_published {

    text-align: center;
    display: inline-block;
    padding:2px 5px;
    
}
.pub_thumbnail {
    
   border: 5px solid #cecece;
    background-color:#cecece;
    border-radius: 0;
    -webkit-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    width: 100px;
    height: 100px;
    float:right;
    overflow:hidden;
    white-space: nowrap;
    text-align:center;
		
}

.pub_thumbnail img {
	vertical-align: middle;
	height:90px;
        
        	display: inline-block;

		
}
    </style>

    @{group@
    @?groupkey@<h3 class="papercite" id=@groupkey@>@groupkey@</h3>@;groupkey@
    <ul class="papercite_bibliography" style="padding-left: 0px;">
     @{entry@ 
        
            <div class = "pub_row">
                
                
                    <div class = "pub_thumbnail" id = 'imgCont@key@' ></div>
               
                <div align="left" >
                        <div align="justify"  style="padding-right: 110px;">
                        <a href= WP_ROOTFOLDER/publications/@cite@>
                            @#entry@

                            @?howpublished@
                            @howpublished@
                            @;howpublished@

                        </a>
                       </div>
                </div>
                <div class="pub_published" id="thumbnail@key@" ></div>
                @?pdf@ <a href="@PAPERCITE_DATA_URL@/@pdf@" title='Download PDF' class='papercite_pdf'><img src='@WP_PLUGIN_URL@/papercite/img/pdf.png' alt="[PDF]"/></a>@;pdf@
                @?doi@<a href='http://dx.doi.org/@doi@' class='papercite_doi' title='View document on publisher site'><img src='@WP_PLUGIN_URL@/papercite/img/external.png' width='10' height='10' alt='[DOI]' /></a>@;doi@
                @?vid@ <a href="@PAPERCITE_DATA_URL@/@vid@" title='Download Video' class='papercite_pdf'><img src='@PAPERCITE_DATA_URL@/images/video.png' height="16px" alt="[VID]"/></a>@;vid@
                @?youtube@ <a href="@youtube@" title='Download Video' class='papercite_pdf'><img src='@PAPERCITE_DATA_URL@/images/youtube.png' height="16px" alt="[YT]"/></a>@;youtube@
                
                <a href="javascript:void(0)" id="papercite_@papercite_id@" class="papercite_toggle">[Bibtex]</a>
                <div class="papercite_bibtex" id="papercite_@papercite_id@_block"><pre><code class="tex bibtex">@bibtex@</code></pre></div>
                
                
            </div>
        
        <div style="clear: both;" />
        </div>
        
        <script>
            
            var thumbString = "@thumbnails@";
            var thumbnails = thumbString.split(",");
            var container = document.getElementById("imgCont"+"@key@");

            var thumbnail = document.createElement("img");
            thumbnail.src = '@PAPERCITE_DATA_URL@/'+thumbnails[0];
            thumbnail.style= "display:inline-block;";

            container.appendChild(thumbnail);
            
            var thumbCont = document.getElementById("thumbnail@key@");
            thumbCont.style.backgroundColor = "#cecece";
            
            var colors = ["#b0b0b0","c0c0c0","#d0d0d0","#e0e0e0","#f0f0f0"];
            if ("@howpublished@"){
                if (/tutorial/i.test("@howpublished@"))
                {   
                    thumbCont.style.backgroundColor = colors[3];
                    thumbCont.innerHTML="Tutorial";
                }
                else if (/talk/i.test("@howpublished@"))
                {   
                    thumbCont.style.backgroundColor = colors[2];
                    thumbCont.innerHTML="Invited Talk";
                }
                else if (/report/i.test("@howpublished@"))
                {
                    thumbCont.style.backgroundColor = colors[4];
                    thumbCont.innerHTML="Report";
                }
                else if (/poster/i.test("@howpublished@"))
                {
                    thumbCont.style.backgroundColor =colors[1];
                    thumbCont.innerHTML="Poster";
                }
                else if (/presentation/i.test("@howpublished@"))
                {
                    thumbCont.style.backgroundColor =colors[3];
                    thumbCont.innerHTML="Presentation";
                }
                else if (/abstract/i.test("@howpublished@"))
                {
                    thumbCont.style.backgroundColor =colors[3];
                    thumbCont.innerHTML="Abstract";
                }
                else
                {
                    thumbCont.style.backgroundColor =colors[4];
                    thumbCont.innerHTML="@howpublished@";
                }
            }
            else
            { 
                
                if (/inproceedings/i.test("@entrytype@"))
                {
                    thumbCont.style.backgroundColor =colors[0];
                    thumbCont.innerHTML="Conference Paper";
                }
                else if (/article/i.test("@entrytype@"))
                {
                    thumbCont.style.backgroundColor =colors[0];
                    thumbCont.innerHTML="Journal Article";
                }
                else if (/book/i.test("@entrytype@"))
                {
                    thumbCont.style.backgroundColor =colors[0];
                    thumbCont.innerHTML="Book";
                }
                else if (/incollection/i.test("@entrytype@"))
                {
                    thumbCont.style.backgroundColor =colors[0];
                    thumbCont.innerHTML="Book Chapter";
                }
                else if (/phdthesis/i.test("@entrytype@"))
                {
                    thumbCont.style.backgroundColor =colors[0];
                    thumbCont.innerHTML="PhD. Thesis";
                }
                else if (/masterthesis/i.test("@entrytype@"))
                {
                    thumbCont.style.backgroundColor =colors[0];
                    thumbCont.innerHTML="Master Thesis";
                }
            }
            
        </script>
     @}entry@ 
	
    </ul>
    @}group@

</div>
</article>
</div>
    

