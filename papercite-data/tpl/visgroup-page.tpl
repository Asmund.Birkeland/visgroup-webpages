@{group@
@?groupkey@
<h3 class="papercite">@groupkey@</h3>
@;groupkey@
@{entry@
<style>
    
    

   
    .pub_image {
        float:right;
        border: 5px solid #cecece;
        
        height:100%;
        width:30%;
        max-height: 200px;
        overflow: hidden;
    }
    
    
</style>
    <header class="entry-header page-title pad group" > 
        <div >
            <div class="pub_image" id = "imgMain@key@"></div>
        
            <h1 class="entry-title">@title@</h1>
        </div>
        <h2><span><div  id="authors">@author@</div></span></h2>
         
        <link href="http://vjs.zencdn.net/6.2.8/video-js.css" rel="stylesheet">
        
    </header>
    <div class="entry-content pad group">
        <article>
            <div class="entry themeform">
                <div class="row" >
                    <div id="abstract" class="col-md-12 col-sm-12">
                        <div style="padding-top:10px;">
                            <blockquote>
                            <h2>Abstract</h2>
                            <p align="justify">
                            @abstract@
                            </p>
                            </blockquote>
                        </div>
                        
                    </div>
                   

                    <div class="col-md-12">
                        <p align="justify">
                            @#entry@
                            @?doi@
                            <a href='http://dx.doi.org/@doi@' class='papercite_doi' title='View on publisher site'>doi:@doi@</a>
                            @;doi@
                            <br/>
                            <a href="javascript:void(0)" id="papercite_@papercite_id@" class="papercite_toggle">[BibTeX]</a>
                            @?abstract@
                            <blockquote class="papercite_bibtex" id="papercite_abstract_@papercite_id@_block">@abstract@</blockquote>
                            @;abstract@
                            <div class="papercite_bibtex" id="papercite_@papercite_id@_block">
                                <pre><code class="tex bibtex">@bibtex@</code></pre>
                            </div>
                            <div style='display:none;'>
                                projectid@project@projectid
                            </div>
                        </p>
                    </div>
                    @?images||vid@
                    <div id ="media" class="col-md-12" >
                        <h2>Media</h2>  
                        <div id = 'imgCont@key@'></div>
                        
                        
                        
                    </div>
                    @;images||vid@
                    @?url||pdf||pres@
                    
                    <div id="Downloads" class="col-md-12">
                        
                        <h2>Downloads</h2>
                        
                        @?pdf@
                            <a href="@PAPERCITE_DATA_URL@/@pdf@" title='Download PDF' class='papercite_pdf'>
                                Full paper
                                <img src='@WP_PLUGIN_URL@/papercite/img/pdf.png' alt="[PDF]"/>
                            </a>
                        @:pdf@
                        
                            @?url@
                            <a href="@url@" title='Download PDF' class='papercite_pdf'>[Download PDF]</a>
                            @;url@
                        @;pdf@
                       
                        
                        @?pres@ 
                        <a href="@PAPERCITE_DATA_URL@/@pres@" title='Download PDF' class='papercite_pdf'>
                            Presentation
                            <img src='@WP_PLUGIN_URL@/papercite/img/pdf.png' alt="[PDF]"/>
                        </a>
                        @;pres@
                        
                    </div>
                    @;url||pdf||pres@
                    
                    @?git@
                    <div id="source" class="col-md-12">
                      <h2>Source Code</h2>
                      <a href="@git@">@git@</a>
                    </div>
                    @;git@
                </div>
            </div>
        </article>
        
  </div>

<script>
    var thumbString = "@images@";
    var thumbnails = thumbString.split(",");
    var abstractWidth = document.getElementById("abstract").offsetWidth;
    var abstractHeight = document.getElementById("abstract").offsetHeight;
    
    document.getElementById('media').style.visibility = "hidden";
    for (i in thumbnails)
    {
        var thumbnail = document.createElement("img");
        thumbnail.id=thumbnails[i];
        
        thumbnail.src = '@PAPERCITE_DATA_URL@/'+thumbnails[i].replace(" ","");
        
        
        var img_link = document.createElement("a");
        img_link.classList.add("fancybox");
        img_link.appendChild(thumbnail);
        img_link.href = thumbnail.src;
        
        if (i == 0) 
        {
            var image_style= 'padding:5px;max-height:50%;max-width:100%';
            
            thumbnail.style=image_style;
            var imgcontainer = document.getElementById("imgMain"+"@key@");
            imgcontainer.appendChild(img_link);
        }
        else
        {  
            document.getElementById('media').style.visibility = "visible";
            var image_style= 'padding:5px;height:100%';
            thumbnail.style=image_style;
           
            var imgcontainer = document.getElementById("imgCont"+"@key@");
            imgcontainer.style = "height:100px";
            imgcontainer.appendChild(img_link);
        }  
    }
   
    
    if ("@youtube@") {
        document.getElementById('media').style.visibility = "visible";
        var default_link = "https://www.youtube.com/embed/";
        var original_link = "https://www.youtube.com/watch?v=";
        var vidListString = "@youtube@";
        var vidList = vidListString.split(",");
        
        for (i in vidList)
        {
          var vidString = vidList[i];
          var vidID = vidString.replace(original_link,"");

          var vidEmbedLink = vidString.replace(original_link,default_link);

          var thumbLink = "https://img.youtube.com/vi/"+vidID+"/1.jpg";
          var button = document.createElement("img");
          button.src = thumbLink;

          var vid_link = document.createElement("a");
          vid_link.classList.add("fancybox");
          vid_link.classList.add("iframe");
          vid_link.appendChild(button);
          vid_link.href = vidEmbedLink;

          var imgcontainer = document.getElementById("imgCont"+"@key@");
          imgcontainer.style = "height:100px";
          imgcontainer.appendChild(vid_link);
        }
    }
     if ("@vid@") {
        document.getElementById('media').style.visibility = "visible";
        
        var vidListString = "@vid@";
        var vidList = vidListString.split(",");
        
        for (i in vidList)
        {
          var vidString = vidList[i];       
          var vidName = vidString.split('.')[0];

          var button = document.createElement("img");

          button.src = '@PAPERCITE_DATA_URL@/images/video.jpg';
          button.width=100;
          button.height=100;
          var vid_link = document.createElement("a");
          vid_link.classList.add("fancybox");
          vid_link.classList.add("iframe");
              
          vid_link.appendChild(button);
         
          vid_link.href = '@PAPERCITE_DATA_URL@/@vid@';

          var imgcontainer = document.getElementById("imgCont"+"@key@");
          imgcontainer.style = "height:100px";
          imgcontainer.appendChild(vid_link);
        }
    }
    
</script>



@}entry@
@}group@
