<?php
/*
Plugin Name: Bibtex-editor
Plugin URI:   
Description:  Editor for bibtex file stored in papercite
Version:      20171106
Author:       Åsmund Birkeland
Author URI:   http://vis.h.uib.no/team/asmund-birkeland/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wporg
Domain Path:  /languages
*/
/*
{Bibtex-editor} is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
{Bibtex-editor} is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {URI to Plugin License}.
*/

function plugin_activated()
{
     pulications_menu_add_page();
}
add_action('admin_menu', 'plugin_activated');
function plugin_deactivated()
{
    
}

function plugin_uninstall()
{
    
}

register_activation_hook( __FILE__, 'plugin_activated' );
register_deactivation_hook( __FILE__, 'plugin_deactivated' );
register_uninstall_hook( __FILE__, 'plugin_uninstall' );

function pulications_menu_add_page() {
    add_menu_page('Publications Page', 'Edit publications', 'manage_options', 'publications_menu', 'publications_menu_page');
}

// display the admin options page

function publications_menu_page() {
    $file = get_option('publication_file'); 
    if(!$file) {
        $file = 'publications.bib';
    }
    $publicationPath = "";#get_option('publication_file_path');
    
    $siteURL = get_site_url().'/';
    $sitePath = get_home_path();
    
   
    
    if (!$publicationPath){
        $publicationPath = "wp-content/papercite-data/bib/";
    }
    $fullFileURL = $siteURL . $publicationPath . $file;
    
    
    $existingDatabaseEntry = get_option('publication_database');
    
    $fileEntry = readFromFile($sitePath . $publicationPath . $file);
    
     include 'editor.php';
    
}

function publications_init() {
    register_setting('publication-settings', 'publications-info', 'validate_publication');
    add_settings_section('publication-main', 'How to', 'publication_entry_description', 'publication');
    add_settings_field('plugin_text_string', 'Bibtex entry', 'publications_entry_html', 'publication', 'publication-main');
}
function publications_entry_html() {
    
    $options = get_option('publication');
    $fileInput = readFromFile('test.txt');
    echo "<textarea id='publication_text_string' name='publications[text_string]' rows='20' cols='50' value='".$fileInput. "' ></textarea>";
}
function publication_entry_description() {
    echo '<p>Main description of this section here.</p>';
}
function validate_publication($input ) {
   
    // Say our second option must be safe text with no HTML tags
    do_action('save_file','test.txt', 'test');
    saveFile('test.txt', 'test2');
    return $input;
}

function readFromFile($filename)
{
    
    $myfile = fopen($filename, "r");
    if (!$myfile)
    {
        echo $filename . " not found";
        return "";
    }
    $output='';
    if(filesize($filename))
        $output =  fread($myfile,filesize($filename));
    
    fclose($myfile);
    return "$output";

}
function remove_footer_admin () 
{
    echo '<span id="footer-thankyou"></span>';
}
 
add_filter('admin_footer_text', 'remove_footer_admin')
?>
