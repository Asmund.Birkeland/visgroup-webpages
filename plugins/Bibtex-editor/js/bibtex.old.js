;var bibtexApp = (function(bibtexData, angular, _, bibtexParse){
    'use strict';

    function BibtexAttribute() {
        var self = this;

        self.name = '';
        self.type = '';
        self.value = '';
        self.required = false;
    }

    function BibtexEntryType() {
        var self = this;

        self.attributes = [];
        self.name = '';
    }

    function Option(name, value) {
        var self = this;

        self.name = name || '';
        self.value = value || false;
    }


    var app = angular.module('bibtexApp', ['ui.sortable', 'camelCaseToHuman']);

    app.controller('BibtexController', function($scope, $http, $animate) {

        var advanced = new Option('Advanced editing', false),
            quotes = new Option('Quote with curly brackets', false);

         
        
        $scope.selectedEntryType = null;
        $scope.currentEntry = null;
        $scope.entryTypes = null;
        $scope.indent = '    ';
        $scope.options = [];
        $scope.databaseImported = false;
        $scope.databaseWP = "";
        $scope.databaseFile = "";
        $scope.options.push(advanced);
        $scope.options.push(quotes);

        $scope.clear = function() {
            replaceCurrentEntry(angular.copy($scope.selectedEntryType));
        };

        $scope.setActiveEntryType = function(et) {
            $scope.selectedEntryType = et;
            $scope.clear();
        };

        $scope.generateKey = function(entry) {
            var author = _(entry.attributes).where(function(n) { return n.name === 'Author'; }).first().value.toLowerCase() || '';
            var year = _(entry.attributes).where(function(n) { return n.name === 'Year'; }).first().value || '';

            entry.key = author.replace(/ |\,|-|\,|\band\b/g,'').toLowerCase() + year;
        };

        $scope.getType = function(name) {
            if(advanced.value === true) {
                return name + '-advanced';
            }

            return name;
        };

        $scope.listOfEntries = [];
        $scope.listOfNewEntries = [];
        $scope.entryFilter = '';

        $scope.setCurrentEntry = function(entry) {
            $scope.currentEntry = entry;
            $scope.selectedEntryType = getPrototypeForEntity(entry);
            
           
        };

        $scope.newEntry = function() {
            addNewEntry(angular.copy($scope.selectedEntryType));
        };

        $scope.deleteEntry = function(entry) {
            var i = getIndexOfEntry(entry);

            $scope.listOfEntries.splice(i, 1);
            if($scope.currentEntry === entry) {
                i = Math.max(0, i - 1);
                if($scope.listOfEntries.length === 0) {
                    $scope.newEntry();
                }

                $scope.setCurrentEntry($scope.listOfEntries[i]);
            }
        };
        
        $scope.importText = '';

        $scope.importMessage = function() {
            
            try {
                return 'Containing ' + bibtexParse.toJSON($scope.importText).length + ' entries.';
            }
            catch(e) {
                return 'Not able to parse.';
            }

        };
        
        

        $scope.import = function() {
            
            importEntries($scope.importText);
                
            $scope.importText = '';
        };
        
        $scope.importFromDatabase = function() {
            console.log(importFromDB)
            console.log(importFromFile)
            if(importFromFile){
                importEntries($scope.databaseFile);
                $scope.databaseImported = true;
            }
            if(importFromDB){
                importEntries($scope.databaseDB);
                $scope.databaseImported = true;
            }
            
            
        };
        
        $scope.entryExists = function(key) {
            for(var i in $scope.listOfEntries) {
                if($scope.listOfEntries[i].key === key) {
                    return true;
                }
            }
            return false;
        }

        $scope.export = function() {
            $scope.exportAll = exportEntries($scope.listOfEntries);
        };
        $scope.exportAll = '';
        
       
        $scope.exportAllEntries = function() {
            document.getElementById("publications_combined").value = "";
            if(!$scope.databaseImported)
            {
                $scope.importFromDatabase();
                
            }
            
            $scope.exportAll = exportEntries($scope.listOfEntries);
            document.getElementById("publications_combined").value = $scope.exportAll;
            
        };
        $scope.exportAll = '';
        
        $scope.submitForm=function(){
            if(checkFile())
            {
                
                alert("File has changed");
                importEntries(readFile());
            }
           
            $scope.exportAllEntries();
            
            if(!document.getElementById("publications_combined").value)
            {
                if( confirm("Copying nothing. This will erase the database!"))
                {
                    document.getElementById("saveData").submit();
                }
            }
            else
            {
                document.getElementById("saveData").submit();
            }
            
        };
        $scope.addFilesToEntry = function(outputId)
        {

            var inputId = outputId + "ToUpload";
            var input = document.getElementById(inputId);
            var output = "";
            var entryInput = document.getElementById(outputId);
            for (var i = 0; i < input.files.length; i++) {

                output += outputId + "/" + input.files[i].name + ', ';
            }
            entryInput.value = output;
            $scope.output();

        }

        $scope.output = function() {
            
            
            setAutoComplete($scope.currentEntry);
            return exportEntry($scope.currentEntry);
        };
        
       
        $scope.getAllFromThisKey = function(key) {
            return _($scope.listOfEntries).where(function(n){ return _(n.attributes).where(function(n){ return n.name === key; }).any(); })
                                          .select(function(n) { return _(n.attributes).where()[key]; })
                                          .values();
        };

        $scope.isUnfinished = function() {
            return $scope.isEntryUnfinished($scope.currentEntry)

            
        };
        
        $scope.isEntryUnfinished = function(entry) {
            if(entry !== null) {
                var items = entry.attributes;
                for(var i in items) {
                    if(items[i].required && (items[i].value === undefined || items[i].value === '')) {
                        return true;
                    }
                }
            }

            return false;
        };
        
         function setAutoComplete(entry)
        {
            
            //console.log(exportEntry($scope.currentEntry));
            
           var items = entry['attributes'];
            for(var i in items) {
                var type = items[i]['name'];
                
                if (type in window.bibtex.autocomplete)
                {
                    var data = window.bibtex.autocomplete[type];
                    $("#"+type).autocomplete({source:data});
                }
            }
            
            
        };
        
       

        function getIndexOfEntry(entry) {
            for(var i in $scope.listOfEntries) {
                if($scope.listOfEntries[i] === entry) {
                    return i;
                }
            }
            return -1;
        }

        function getPrototypeForEntity(entity) {
            for(var i in $scope.entryTypes) {
                if(entity.name === $scope.entryTypes[i].name) {
                    return $scope.entryTypes[i];
                }
            }
        }

        function replaceCurrentEntry(newEntry) {
            var i = getIndexOfEntry($scope.currentEntry);
            if(i === -1) {
                throw 'Current entry could not be found';
            }

            $scope.listOfEntries.splice(i,1, newEntry);

            $scope.currentEntry = newEntry;
        }

        function addNewEntry(type) {
            $scope.listOfEntries.unshift(type);
            
            $scope.setCurrentEntry(type);
        }

        function importEntries(bibtex) {
            
            var json = bibtexParse.toJSON(bibtex);

            for(var i = 0, length = json.length; i < length; i++) {
                var current = json[i];

                var type = _($scope.entryTypes).where(function(n){ return n.name.toLowerCase() === current.entryType.toLowerCase(); }).first();

                if(type) {
                    var newEntry = angular.copy(type);
                    newEntry.key = current.citationKey;
                    if(entryExist(newEntry))
                        continue;

                    for(var attr in current.entryTags) {
                        setAttribute(newEntry, attr, current.entryTags[attr]);
                    }
                    //$scope.listOfEntries.unshift(newEntry);
                    $scope.listOfEntries.push(newEntry);
                    
                }
                else {
                    console.log('Not able to find entry type: ' + current.entryType);
                }

            }
            sortEntries();
        }
        
        function sortEntries(){
            $scope.listOfEntries.sort(function(a, b) { 
                 return getYear(b) - getYear(a);
            })
        }
        
        function getYear(entry)
        {
            var items = entry['attributes'];
            for (var i in items){
                if (items[i]['name'] == 'Year')
                {
                    return items[i]['value'];
                }
            }
            return -1;
        }
        
        function entryExist(entry){
            for(var i = 0, length = $scope.listOfEntries.length; i < length; i++) {
                if($scope.listOfEntries[i].key == entry.key) {
                    return true;
                }
            }
            return false;
        }

        function setAttribute(entry, attr, val) {
            var attrL = attr.toLowerCase();
            val = val.replace(/\n/g, '');
            var tag = _(entry.attributes).where(function(n) { return n.name.toLowerCase() === attrL; });

            if(tag.any()) {
                tag.first().value = val;
            }
            else {
                var unknownAttr = new BibtexAttribute();
                unknownAttr.name = attr;
                unknownAttr.type = 'string';
                unknownAttr.value = val;

                entry.attributes.push(unknownAttr);
            }
        }

        function exportEntries(entries) {
            var output = '';
            for(var i = 0, length = entries.length; i < length; i++) {
                if(!$scope.isEntryUnfinished(entries[i]) && entries[i].export) {
                    output += exportEntry(entries[i]) + '\n';
                }
            }

            return output;
        }

        function exportEntry(entry) {
            
            function padToLength(str, length) {
                if(str.length < length) {
                    return padToLength(str + ' ', length);
                }
                else {
                    return str;
                }
            }

            if(entry !== null) {
                var output = '@' + entry.name.toUpperCase() + ' {' + (entry.key || '') + ',\n',
                    items = entry.attributes,
                    startQuote = quotes.value ? '{': '"',
                    endQuote = quotes.value ? '}': '"';

                var longest = 0;
                for(var i in items) {
                    if(items[i].value && items[i].value !== '') {
                        longest = longest < items[i].name.length ? items[i].name.length : longest;
                    }
                }

                for(i in items) {
                    if(items[i].value && items[i].value !== '') {
                        output += $scope.indent + padToLength(items[i].name.toLowerCase(), longest) + ' = ' + startQuote + items[i].value + endQuote + ',\n';
                    }
                }

                return output.substr(0, output.length - ',\n'.length) + '\n' + '}';
            }

            return '';
        }
        function checkFile() {
            var result = readFile()
            
            
            return (result != $scope.databaseFile)
            
                
        }
         function readFile() {
            var filePath =$scope.filePath
            var result = null;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", filePath, false);
            xmlhttp.send();
            if (xmlhttp.status==200) {
              result = xmlhttp.responseText;
            }
            
            
            return result;
            
                
        }

        function init(data, database, file, filepath    ) {
            
            var entriesKey = 'bibtex-entries';

            // Inits the different entry types
            $scope.entryTypes = data.entityTypes;
            $scope.filePath = filepath;
            
            for(var i in $scope.entryTypes) {
                $scope.entryTypes[i].key = '';
                $scope.entryTypes[i].export = true;
            }
            $scope.databaseDB = database;
            $scope.databaseFile = file;
            $scope.selectedEntryType = $scope.entryTypes[0];

            // Loads the entries from local storage if present, or create a new entry
            
//            var success = importEntries(database);
//            if(database != file)
//            {
//                importEntries(file);
//            }
//            
//            if(success)
//            {
//                $scope.setCurrentEntry($scope.listOfEntries[0]);
//            }
//            else
            {
            $scope.newEntry();
            }
          
            
            // Sets up saving of the model
//            $scope.$watch('listOfEntries', function(newVal, oldVal, $scope) {
//                $scope.exportNewEntries();
//            }, true);
        }

        if(bibtexData) {
            init(bibtexData, dbEntry, fileEntry, fileURL);   
        }
        else {
            alert('Could not find data!');
        }
    });
    
    return app;
})(window.bibtex, angular, _, bibtexParse);