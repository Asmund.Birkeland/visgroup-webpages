class bibtexEntry {
    constructor(){
        self.ID = '';
        self.attributes = [];
    }
}


class bibtexHandler{
    constructor()
    {
         
        self.importDatabase = '';
        self.selectedEntryType = null;
        self.currentEntry = null;
        self.entryTypes = null;
        self.indent = '    ';
        self.options = [];
        
        self.listOfEntries = [];
        self.entryFilter = '';
        self.importText = '';
    }

    bibtexAttribute() {
 
        self.name = '';
        self.type = '';
        self.value = '';
        self.required = false;
    }

    BibtexEntryType() {
        

        self.attributes = [];
        self.name = '';
    }

    Option(name, value) {
        

        self.name = name || '';
        self.value = value || false;
    }



      clear() {
            replaceCurrentEntry(self.selectedEntryType);
        };

        setActiveEntry(et) {
            self.selectedEntryType = et;
            
        };

        generateKey(entry) {
            var author = _(entry.attributes).where(function(n) { return n.name === 'Author'; }).first().value.toLowerCase() || '';
            var year = _(entry.attributes).where(function(n) { return n.name === 'Year'; }).first().value || '';

            entry.key = author.replace(/ |\,|-|\,|\band\b/g,'').toLowerCase() + year;
        };

        getType(name) {            
            return name;
        };

        

        setCurrentEntry(entry) {
            self.currentEntry = entry;
            self.selectedEntryType = getPrototypeForEntity(entry);
        };

        newEntry(){
            addNewEntry(angular.copy($scope.selectedEntryType));
        };

        deleteEntry(entry) {
            var i = getIndexOfEntry(entry);

            self.listOfEntries.splice(i, 1);
            if(self.currentEntry === entry) {
                i = Math.max(0, i - 1);
                if(self.listOfEntries.length === 0) {
                    self.newEntry();
                }

                self.setCurrentEntry(self.listOfEntries[i]);
            }
        };
        
        

        importMessage() {
            
            try {
                return 'Containing ' + bibtexParse.toJSON(self.importText).length + ' entries.';
            }
            catch(e) {
                return 'Not able to parse.';
            }

        };
        
        

        import() {
            console.log("bibtex");
            importEntries(self.importText);
            
            self.importText = '';
        };

        export() {
            $scope.exportAll = exportEntries($scope.listOfEntries);
        };

        
        output() {
            return exportEntry($scope.currentEntry);
        };

        getAllFromThisKey(key) {
            return _(self.listOfEntries).where(function(n){ return _(n.attributes).where(function(n){ return n.name === key; }).any(); })
                                          .select(function(n) { return _(n.attributes).where()[key]; })
                                          .values();
        };

        isUnfinished() {
            if(self.currentEntry !== null) {
                var items = self.currentEntry.attributes;
                for(var i in items) {
                    if(items[i].required && (items[i].value === undefined || items[i].value === '')) {
                        return true;
                    }
                }
            }

            return false;
        };

        getIndexOfEntry(entry) {
            for(var i in $scope.listOfEntries) {
                if($scope.listOfEntries[i] === entry) {
                    return i;
                }
            }
            return -1;
        }

        getPrototypeForEntity(entity) {
            for(var i in $scope.entryTypes) {
                if(entity.name === $scope.entryTypes[i].name) {
                    return $scope.entryTypes[i];
                }
            }
        }

        replaceCurrentEntry(newEntry) {
            var i = getIndexOfEntry(self.currentEntry);
            if(i === -1) {
                throw 'Current entry could not be found';
            }

            self.listOfEntries.splice(i,1, newEntry);

            self.currentEntry = newEntry;
        }

        addNewEntry(type) {
            self.listOfEntries.push(type);
            self.setCurrentEntry(type);
        }

        importEntries(bibtex) {
            
            var json = bibtexParse.toJSON(bibtex);

            for(var i = 0, length = json.length; i < length; i++) {
                var current = json[i];

                var type = _(self.entryTypes).where(function(n){ return n.name.toLowerCase() === current.entryType.toLowerCase(); }).first();

                if(type) {
                    var newEntry = angular.copy(type);
                    newEntry.key = current.citationKey;

                    for(var attr in current.entryTags) {
                        setAttribute(newEntry, attr, current.entryTags[attr]);
                    }

                    self.listOfEntries.push(newEntry);
                }
                else {
                    console.log('Not able to find entry type: ' + current.entryType);
                }

            }
        }

        setAttribute(entry, attr, val) {
            var attrL = attr.toLowerCase();
            val = val.replace(/\n/g, '');
            var tag = _(entry.attributes).where(function(n) { return n.name.toLowerCase() === attrL; });

            if(tag.any()) {
                tag.first().value = val;
            }
            else {
                var unknownAttr = new BibtexAttribute();
                unknownAttr.name = attr;
                unknownAttr.type = 'string';
                unknownAttr.value = val;

                entry.attributes.push(unknownAttr);
            }
        }

        exportEntries(entries) {
            var output = '';
            for(var i = 0, length = entries.length; i < length; i++) {
                if(entries[i].export) {
                    output += exportEntry(entries[i]) + '\n';
                }
            }

            return output;
        }

        exportEntry(entry) {

            function padToLength(str, length) {
                if(str.length < length) {
                    return padToLength(str + ' ', length);
                }
                else {
                    return str;
                }
            }

            if(entry !== null) {
                var output = '@' + entry.name.toUpperCase() + ' {' + (entry.key || '') + ',\n',
                    items = entry.attributes,
                    startQuote = quotes.value ? '{': '"',
                    endQuote = quotes.value ? '}': '"';

                var longest = 0;
                for(var i in items) {
                    if(items[i].value && items[i].value !== '') {
                        longest = longest < items[i].name.length ? items[i].name.length : longest;
                    }
                }

                for(i in items) {
                    if(items[i].value && items[i].value !== '') {
                        output += $scope.indent + padToLength(items[i].name.toLowerCase(), longest) + ' = ' + startQuote + items[i].value.replace(/"/g, '\\"').replace(/&/g, '\\&') + endQuote + ',\n';
                    }
                }

                return output.substr(0, output.length - ',\n'.length) + '\n' + '}';
            }

            return '';
        }

        init(data) {
            var entriesKey = 'bibtex-entries';

            // Inits the different entry types
            self.entryTypes = data;
            for(var i in self.entryTypes) {
                self.entryTypes[i].key = '';
                self.entryTypes[i].export = true;
            }

            self.selectedEntryType = self.entryTypes[0];

            // Loads the entries from local storage if present, or create a new entry
            if(localStorage.getItem(entriesKey)) {
                self.listOfEntries = localStorage.getItem(entriesKey);
                self.currentEntry = $scope.listOfEntries[0];
            }
            else {
                self.newEntry();
            }


        }
        
        
}
    