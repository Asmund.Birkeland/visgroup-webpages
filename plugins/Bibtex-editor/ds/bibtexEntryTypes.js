window.bibtex = window.bibtex || {};
window.bibtex.entityTypes = 
[
    {
        "name": "Article",
        "desc": "An article from a journal or magazine.",
        "attributes": [
            {"name": "Author", "type": "autocomplete", "required": true},
            {"name": "Title", "type": "string", "required": true},
            {"name": "Journal", "type": "string", "required": true},
            {"name": "Year", "type": "year", "required": true},
            
            {"name": "Volume", "type": "string", "required": false},
            {"name": "Number", "type": "string", "required": false},
            {"name": "Pages", "type": "pages", "required": false},
            {"name": "Month", "type": "month", "required": false},
            
            {"name": "Abstract", "type": "note", "required": false},
            {"name": "pdf", "type": "file", "required": false},
            {"name": "vid", "type": "file", "required": false},
            {"name": "images", "type": "file", "required": false},
            {"name": "thumbnails", "type": "file", "required": false},
            {"name": "youtube", "type": "string", "required": false},
           
            {"name": "Crossref", "type": "crossref", "required": false},
            {"name": "Note", "type": "note", "required": false},
            
        ]
    },
    {
        "name": "Book",
        "desc": "A book with an explicit publisher.",
        "attributes": [
            {"name": "Author", "type": "autocomplete", "required": true},
            {"name": "Title", "type": "string", "required": true},
            {"name": "Publisher", "type": "autocomplete", "required": true},
            {"name": "Year", "type": "year", "required": true},
            
            {"name": "Volume", "type": "string", "required": false},
            {"name": "Series", "type": "string", "required": false},
            {"name": "Address", "type": "string", "required": false},
            {"name": "Edition", "type": "edition", "required": false},
            {"name": "Month", "type": "month", "required": false},
            
            {"name": "Abstract", "type": "note", "required": false},
            {"name": "pdf", "type": "file", "required": false},
            {"name": "vid", "type": "file", "required": false},
            {"name": "images", "type": "file", "required": false},
            {"name": "thumbnails", "type": "file", "required": false},
            {"name": "youtube", "type": "string", "required": false},
            
            {"name": "Crossref", "type": "crossref", "required": false},
            {"name": "Note", "type": "note", "required": false}
        ]
    },
    {
        "name": "InCollection",
        "desc": "A part of a book having its own title.",
        "attributes": [
            {"name": "Author", "type": "autocomplete", "required": true},
            {"name": "Title", "type": "string", "required": true},
            {"name": "BookTitle", "type": "string", "required": true},
            {"name": "Publisher", "type": "autocomplete", "required": true},
            {"name": "Year", "type": "year", "required": true},
            
            {"name": "Editor", "type": "string", "required": false},
            {"name": "Volume", "type": "string", "required": false},
            {"name": "Series", "type": "string", "required": false},
            {"name": "Type", "type": "string", "required": false},
            {"name": "Chapter", "type": "string", "required": false},
            {"name": "Pages", "type": "pages", "required": false},
            {"name": "Address", "type": "string", "required": false},
            {"name": "Edition", "type": "edition", "required": false},
            {"name": "Organization", "type": "string", "required": false},
            {"name": "Publisher", "type": "autocomplete", "required": false},
            {"name": "Month", "type": "month", "required": false},
            
            {"name": "Abstract", "type": "note", "required": false},
            {"name": "pdf", "type": "file", "required": false},
            {"name": "vid", "type": "file", "required": false},
            {"name": "images", "type": "file", "required": false},
            {"name": "thumbnails", "type": "file", "required": false},
            {"name": "youtube", "type": "string", "required": false},
            
            {"name": "Crossref", "type": "crossref", "required": false},
            {"name": "Note", "type": "note", "required": false}
        ]
    },
    {
        "name": "InProceedings",
        "desc": "An article in a conference proceedings.",
        "attributes": [
            {"name": "Author", "type": "autocomplete", "required": true},
            {"name": "Title", "type": "string", "required": true},
            {"name": "BookTitle", "type": "string", "required": true},
            {"name": "Year", "type": "year", "required": true},
            
            {"name": "Editor", "type": "string", "required": false},
            {"name": "Volume", "type": "string", "required": false},
            {"name": "Series", "type": "string", "required": false},
            {"name": "Pages", "type": "pages", "required": false},
            {"name": "Address", "type": "string", "required": false},
            {"name": "Month", "type": "month", "required": false},
            {"name": "Organization", "type": "string", "required": false},
            {"name": "Publisher", "type": "autocomplete", "required": false},
            
           {"name": "Abstract", "type": "note", "required": false},
            {"name": "pdf", "type": "file", "required": false},
            {"name": "vid", "type": "file", "required": false},
            {"name": "images", "type": "file", "required": false},
            {"name": "thumbnails", "type": "file", "required": false},
            {"name": "youtube", "type": "string", "required": false},
            
            {"name": "Crossref", "type": "crossref", "required": false},
            {"name": "Note", "type": "note", "required": false}
        ]
    },
    {
        "name": "Conference",
        "desc": "An article in a conference proceedings.",
        "attributes": [
            {"name": "Author", "type": "autocomplete", "required": true},
            {"name": "Title", "type": "string", "required": true},
            {"name": "BookTitle", "type": "string", "required": true},
            {"name": "Year", "type": "year", "required": true},
            
            {"name": "Editor", "type": "string", "required": false},
            {"name": "Volume", "type": "string", "required": false},
            {"name": "Series", "type": "string", "required": false},
            {"name": "Pages", "type": "pages", "required": false},
            {"name": "Address", "type": "string", "required": false},
            {"name": "Month", "type": "month", "required": false},
            {"name": "Organization", "type": "string", "required": false},
            {"name": "Publisher", "type": "autocomplete", "required": false},
            
            {"name": "Abstract", "type": "note", "required": false},
            {"name": "pdf", "type": "file", "required": false},
            {"name": "vid", "type": "file", "required": false},
            {"name": "images", "type": "file", "required": false},
            {"name": "thumbnails", "type": "file", "required": false},
            {"name": "youtube", "type": "string", "required": false},
            
            {"name": "Crossref", "type": "crossref", "required": false},
            {"name": "Note", "type": "note", "required": false}
        ]
    },    
    {
        "name": "MasterThesis",
        "desc": "A Master's thesis.",
        "attributes": [
            {"name": "Author", "type": "autocomplete", "required": true},
            {"name": "Title", "type": "string", "required": true},
            {"name": "School", "type": "string", "required": true},
            {"name": "Year", "type": "year", "required": true},
            
            {"name": "Type", "type": "string", "required": false},
            {"name": "Address", "type": "string", "required": false},
            {"name": "Month", "type": "month", "required": false},
            
            {"name": "Abstract", "type": "note", "required": false},
            {"name": "pdf", "type": "file", "required": false},
            {"name": "vid", "type": "file", "required": false},
            {"name": "images", "type": "file", "required": false},
            {"name": "thumbnails", "type": "file", "required": false},
            {"name": "youtube", "type": "string", "required": false},
            
            {"name": "Crossref", "type": "crossref", "required": false},
            {"name": "Note", "type": "note", "required": false}
            
        ]
    },
    {
        "name": "Misc",
        "desc": "For use when nothing else fits.",
        "attributes": [
            {"name": "Author", "type": "autocomplete", "required": false},
            {"name": "Title", "type": "string", "required": false},
            {"name": "HowPublished", "type": "string", "required": false},
            {"name": "Month", "type": "month", "required": false},
            {"name": "Year", "type": "year", "required": false},
            
            {"name": "Abstract", "type": "note", "required": false},
            {"name": "pdf", "type": "file", "required": false},
            {"name": "vid", "type": "file", "required": false},
            {"name": "images", "type": "file", "required": false},
            {"name": "thumbnails", "type": "file", "required": false},
            {"name": "youtube", "type": "string", "required": false},
            
            {"name": "Crossref", "type": "crossref", "required": false},
            {"name": "Note", "type": "note", "required": false}
        ]
    },
    {
        "name": "PhdThesis",
        "desc": "A Ph.D. thesis.",
        "attributes": [
            {"name": "Author", "type": "autocomplete", "required": true},
            {"name": "Title", "type": "string", "required": true},
            {"name": "School", "type": "string", "required": true},
            {"name": "Year", "type": "year", "required": true},
            
            {"name": "Type", "type": "string", "required": false},
            {"name": "Address", "type": "string", "required": false},
            {"name": "Month", "type": "month", "required": false},
            
            {"name": "Abstract", "type": "note", "required": false},
            {"name": "pdf", "type": "file", "required": false},
            {"name": "vid", "type": "file", "required": false},
            {"name": "images", "type": "file", "required": false},
            {"name": "thumbnails", "type": "file", "required": false},
            {"name": "youtube", "type": "string", "required": false},
            
            
            {"name": "Crossref", "type": "crossref", "required": false},
            {"name": "Note", "type": "note", "required": false}
        ]
    }
];

window.bibtex.autocomplete = 
{
    "Journal":[
        "IEEE Computer Graphics and Applications",
        "Computers \& Graphics",
        "Medical Image Analysis",
        "European Journal of Ultrasound",
        "Ultrasound in Medicine and Biology",
        "The British Journal of Radiology",
        "Lecture Notes in Computer Science",
        "Cardio Vascular and Interventional Radiology",
        "ACM Transactions on Applied Perception",
        "Circulation, Arrhythmia and Electrophysiology",
        "The American Journal of Emergency Medicine",
        "International Journal of Computer Assisted Radiology and Surgery",
        "Computer Graphics Forum",
        "IEEE Transactions on Ultrasonics, Ferroelectrics, and Frequency Control",
        "IEEE Transactions on Medical Imaging",								
        "Medical \& Biological Engineering \& Computing",
        "Transactions on Visualization and Computer Graphics",
        "Progress in Biomedical Optics and Imaging",
        "Computer Animation and Virtual Worlds",
        "Computer Methods and Programs in Biomedicine",
        "SPIE Medical Imaging",
        "ACM Computing Surveys",
        "Current Problems in Diagnostic Radiology",
        "IEEE Transactions on Sonics and Ultrasonics",
        "Ultrasound In Obstetrics \& Gynacology"],
    "Proceedings":[
        "Proceedings of Medical Image Computing and Computer--Assisted Intervention",
        "Proceedings of Medical Image Understanding and Analysis"                    ,  
        "Proceedings of Information Processing in Medical Imaging"                    , 
        "Proceedings of International Conference on Computer Vision"                   ,
        "Proceedings of European Conference on Computer Vision"                        ,
        "Proceedings of Computer Vision and Pattern Recognition"                       ,
        "Proceedings of International Conference of Pattern Recognition"               ,
        "Proceedings of British Machine Vision Conference"                             ,
        "Proceedings of Central European Seminar on Computer Graphics"                 ,

        "Proceedings of ACM SIGGRAPH"                                                  ,
        "Proceedings of Medical Image Computing and Computer--Assisted Intervention"   ,
        "Proceedings of EUROGRAPHICS"                                                  ,
        "Proceedings of Eurographics Workshop on Visual Computing in Biomedicine"      ,

        "Proceedings of IEEE Visualization"                                            ,
        "Proceedings of IEEE Pacific Visualization"                                    ,
        "Proceedings of IEEE Symposium on Volume Visualization"                        ,
        "Proceedings of IEEE Symposium on Information Visualization"                   ,
        "Proceedings of IEEE Engineering in Medicine and Biology Society"              ,

        "Proceedings of Joint Eurographics -- IEEE TCVG Symposium on Visualization"    ,
        "Proceedings of Computer Graphics International"                               ,
        "Proceedings of IEEE Symposium on Visual Analytics Science and Technology"     ,
        "Proceedings of Vision, Modeling, and Visualization"                           ,
        "Proceedings of Visual Computing for Biomedicine"                              ,
        "Proceedings of IEEE/EG International Symposium on Volume Graphics"		,	

        "Proceedings of IEEE Ultrasonics Symposium"					,	

        "Proceedings of Imagery Pattern Recognition Workshop"				,	

        "Proceedings of International Workshop on Volume Graphics"	 	  	,	
        "Proceedings of IEEE International Symposium on Biomedical Imaging: Macro to Nano" ,
        "Proceedings of SPIE"			 	      				  ,	
        "Proceedings of International Conference on Information Processing in Medical Imaging" ,
        "Proceedings of Spring Conference on Computer Graphics" 

    ]
};