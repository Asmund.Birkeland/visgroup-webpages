<?php
#echo "test";
#include '../../../../wp-load.php';
#echo "test2";
?>

<html lang="en" id="ng-application" ng-app="bibtexApp">

    <head>
        <title>BibTeX Online Editor</title>

        <meta name="description" content="Easy to use Bibtex Editor. Keep track of your bibliography!" />
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta http-equiv='Content-Type' content='Type=text/html; charset=UTF8'>

        <link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__); ?>css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__); ?>css/sidebar.css" type="text/css">
        <link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__); ?>css/truben.css" type="text/css">
        <link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__); ?>css/style.css" type="text/css">
        <link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__); ?>css/animations.css" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,200,300' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <link rel="icon" type="image/png" href="http://truben.no/latex/bibtex/img/favicon.png" />
          
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/angular.js" ></script>
        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/lodash.js"></script>
        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/bibtexParse.js"></script>
<!--        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/jquery-ui.js"></script>
        -->
        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/jquery.autofix_anything.min.js"></script>
        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/bootstrap.js"></script>

        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/sortable.js"></script>

        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>ds/bibtexEntryTypes.js"></script>

        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/utils.js"></script>
        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/bibtex.old.js"></script>
        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/camelCaseToHuman.js"></script>
        <script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__); ?>js/Directives/set-focus.js"></script>
        
       

    </head>

    <body id="controller" ng-controller="BibtexController">
        <div class="container-fluid header">
            <div class="" style="height: 80px">
                <div class="pull-left">
                    <h1 class="logo"><span>Bibtex Editor</span></h1>
                </div> 


            </div>
        </div>

        <nav class="navbar navbar-default shadow-under" role="navigation">
            <div class="col-sm-3 col-md-3 col-lg-3">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <form class="navbar-form navbar-left form-search" role="search">
                            <div class="form-group">
                                <input type="text" class="form-control animate-show-width" id="search" placeholder="Search" ng-model="entryFilter" focus-me="showSearch" ng-show="showSearch">
                            </div>
                        </form>
                    </li>
                    <li>
                        <a href="#" ng-click="showSearch = !showSearch; entryFilter = ''" title="Search entries">
                            <i class="fa fa-search" ng-class="{'fa-search': !showSearch, 'fa-times': showSearch}"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9 col-md-9 col-lg-9 main-toolbar">
                <ul class="nav navbar-nav"> 
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Entry type: &nbsp; {{selectedEntryType.name}} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href ng-repeat="e in entryTypes| orderBy: 'name'" onclick="setAutoComplete()" ng-click="setActiveEntryType(e)"><i class="fa blank"></i> {{ e.name}}</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <button type="button" class="navbar-right navbar-toggle pull" data-toggle="collapse" data-target="#advanced-toolbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <ul class="nav navbar-nav navbar-right collapse navbar-collapse" id="advanced-toolbar">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">File <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#export-modal" data-target="#export-modal" data-toggle="modal"><i class="fa fa-download"></i> Export</a></li>
                            <li><a href="#import-modal" data-target="#import-modal" data-toggle="modal"><i class="fa fa-upload"></i> Import</a></li>
                            <li class="divider"></li>
                            <li><a href="#" ng-click="newEntry()" ><i class="fa fa-plus"></i> New Entry</a></li>
                        </ul>
                    </li>

                    <li>&nbsp;</li>
                </ul>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                
                <div class="col-sm-3 col-md-3 col-lg-3 sidebar" >

                    <button type="button" id="importDatabaseButton" class="btn btn-default btn-sm btn-primary" ng-click="importFromDatabase()"  data-dismiss="modal">Import database</button>
                    <br>
                    <br>
                    <button type="button" id="testButton" class="btn btn-default btn-sm btn-primary" ng-click="submitForm()"  >Save database</button>
                    <br>
                    Show database entry: <input type='checkbox' id='show_controllers' name="show_database" onclick="showControllers()" /><br>
                    <div id="controllers">
                        
                        Import from: <br>
                        Database: <input type='checkbox' id='importFromDB' checked onclick="importFrom('importFromDB')" /><br>        
                        File    :  <input type='checkbox' id='importFromFile' checked onclick="importFrom('importFromFile')" /><br>        
                        <button  type="button" class="btn btn-default btn-sm btn-primary" ng-click="exportAllEntries()"  data-dismiss="modal">Export All</button>    
                    </div>

                    <br>
                    <form id="saveData" action="<?php echo plugin_dir_url(__FILE__) . 'save.php'; ?>" method="post"   enctype="multipart/form-data">
                        <input type="submit" class="button-primary" value="Save Changes" />

                        <br>
                        <br>
                        <div id="textInput">
                            File name:
                            <input type="text" id='publication_filename' name='filename' class="autocomplete"  value=<?php echo $file ?> />
                            <br>
                            File path:
                            <input type="text" id='publication_file_path' name='fullpath' hidde='true'  value=<?php echo $publicationPath ?> />
                            <br>
                            old file:<br>
                            <textarea id='publications_old' name='publications_old' rows='25' cols='35' ></textarea>
                            <br>
                            Combined:<br>
                            <textarea id='publications_combined' name='publications_combined' rows='25' cols='35' ></textarea>
                        </div>

                        <input name="images[]" id="imagesToUpload" type="file" multiple="multiple" onchange="addFilesToEntry('images')"/>
                        <input name="thumbnails[]" id="thumbnailsToUpload" type="file" multiple="multiple" onchange="addFilesToEntry( 'thumbnails')"  />
                        <input name="vids[]" id="vidToUpload" type="file" multiple="multiple"  onchange="addFilesToEntry('vid')"/>
                        <input name="pdf[]" id="pdfToUpload" type="file" multiple="multiple"  onchange="addFilesToEntry('pdf')"/>

                    </form>




                    <script type="text/javascript">
                        
                        var importFromDB = true;
                        var importFromFile = true;
                        function importFrom(id){
                            importFromDB = document.getElementById("importFromDB").checked;
                            importFromFile = document.getElementById("importFromFile").checked;
                            
                        }


                        var fileEntry =<?php echo json_encode($fileEntry); ?>;
                        var dbEntry =<?php echo json_encode($existingDatabaseEntry); ?>;
                        var fileURL =<?php echo json_encode($fullFileURL); ?>;




                        hide();
                        function showControllers(){
                            if(document.getElementById("show_controllers").checked){
                                show();
                            }
                            else{
                                hide();
                            }
                        }
                        function show(){   
                                document.getElementById('saveData').style.display = "block";
                                document.getElementById('controllers').style.display = "block";
                        }
                        function hide(){
                                document.getElementById('saveData').style.display = "none";

                                document.getElementById('controllers').style.display = "none";
                        }
                        function addFilesToEntry(outputId)
                        {

                            var inputId = outputId + "ToUpload";
                            var input = document.getElementById(inputId);
                            var output = "";
                            var entryInput = document.getElementById(outputId);
                            var outputFolder = outputId;
                            if(outputId == 'thumbnails')
                                outputFolder = 'images';
                            if(outputFolder == 'pdf' || outputFolder == 'vid')
                                outputFolder = outputFolder + 's';
                            if (!input.files.length)
                                return;
                            output += outputFolder + "/" + input.files[0].name;
                            for (var i = 1; i < input.files.length; i++) {

                                output += "," + outputFolder + "/" + input.files[i].name ;
                            }
                            entryInput.value = output;
                            var event = new Event('change');


                            entryInput.dispatchEvent(event);
                        }


                    </script>


                    <ul class="nav nav-sidebar"  ng-model="listOfEntries">
                        <li>
                            <a ng-click="newEntry()" href="#" class="row" title="Create a new Bibtex entry">
                                <span class="col-xs-12" class="text-muted">
                                    New Entry
                                </span>
                            </a>
                        </li>
                        <li ng-repeat="entry in listOfEntries| filter: entryFilter" ng-class="{ active: (entry == $parent.currentEntry) }" ng-click="setCurrentEntry(entry)">
                            <a href="#" class="row">
                                <span class="col-xs-10">
                                    <i class="fa fa-bars form-control-feedback sorting-handle" title="Click and drag to reorder"></i> &nbsp;
                                    <span>{{ entry.name}}</span>
                                    <span ng-if="entry.key">: {{ entry.key}}</span>
                                </span>
                                <span class="col-xs-2 delete-entry" title="Delete entry" ng-click="deleteEntry(entry); $event.stopPropagation();" >
                                    <i class="fa fa-times-circle"></i>
                                </span>
                            </a>
                        </li>

                    </ul>


                </div>


                   
                
                <div class="col-sm-9 col-md-9 col-lg-9 main">
                    <section>
                        <form class="form-horizontal" role="form">

                            <div class="form-group">
                                <div class="col-lg-1 col-m-1 col-s-1 control-label attr-name">
                                    <label for="key">Key:</label>
                                </div>

                                <div class="col-lg-10 col-m-8 col-s-8">
                                    <div class="input-group">
                                        <input id="key" ng-model="currentEntry.key" class="form-control input-sm" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-default input-sm" ng-click="generateKey(currentEntry)">Generate</button>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div ng-repeat="attr in currentEntry.attributes| orderBy: ['-required'] " class="form-group">
                                <div class="col-lg-1 col-m-1 col-s-1 control-label attr-name">
                                    <label for="{{attr.name}}">{{ attr.name}}:<span ng-show="attr.required" class="required text-danger" title="Required"> *</span>
                                    </label>
                                </div>

                                <div class="col-lg-10 col-m-8 col-s-8" ng-switch="getType(attr.type)">
                                    <span ng-switch-when="year">
                                        <input id="{{attr.name}}" ng-model="attr.value" class="form-control input-sm" placeholder="eg. 2014" style="width: 15%" />
                                    </span>

                                    <span ng-switch-when="pages">
                                        <input id="{{attr.name}}" ng-model="attr.value" class="form-control input-sm" placeholder="e.g., 42-111 or 7,41, 73-97 or 43+" />
                                    </span>

                                    <span ng-switch-when="note">
                                        <textarea id="{{attr.name}}" ng-model="attr.value" class="form-control input-sm"></textarea>
                                    </span>
                                    
                                     <span ng-switch-when="file" class="input-group">
                                        <input id="{{attr.name}}" ng-model="attr.value" class="form-control input-sm" ng-change="output()"/>

                                        <span class="input-group-btn">
                                            <div class="btn-group">
                                                <button type="button" name="{{attr.name}}" class="btn btn-default dropdown-toggle input-sm" onclick="selectFiles()">
                                                    Files
                                                </button>
                                            </div>
                                        </span>
                                    </span>
                                    <script>
                                        function selectFiles()
                                        {
                                            var button = event.target;
                                            var id = button.name + "ToUpload";
                                            console.log(id)
                                            var input = document.getElementById(id);
                                            input.click();
                                            
                                        }
                                       
                                        
                                        
                                    </script>

                                    <span ng-switch-when="crossref" class="input-group">
                                        <input id="{{attr.name}}" ng-model="attr.value" class="form-control input-sm" />

                                        <span class="input-group-btn">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle input-sm" data-toggle="dropdown">
                                                    Reference <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li ng-repeat="entry in $parent.listOfEntries" ng-if="entry.key" ng-click="$parent.attr.value = entry.key"><a href>{{ entry.key}}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </span>
                                    </span>

                                    <span ng-switch-when="month">
                                        <select id="{{attr.name}}" ng-model="attr.value" class="form-control input-sm" style="width: 15%">
                                            <option value="jan" selected>January</option>
                                            <option value="feb">February</option>
                                            <option value="mar">March</option>
                                            <option value="apr">April</option>
                                            <option value="may">May</option>
                                            <option value="jun">June</option>
                                            <option value="jul">July</option>
                                            <option value="aug">August</option>
                                            <option value="sep">September</option>
                                            <option value="oct">October</option>
                                            <option value="nov">November</option>
                                            <option value="dec">December</option>
                                        </select>
                                    </span>

                                    <span ng-switch-when="edition">
                                        <select id="{{attr.name}}" ng-model="attr.value" class="form-control input-sm" style="width: 15%">
                                            <option value="first" selected>First</option>
                                            <option value="second">Second</option>
                                            <option value="third">Third</option>
                                            <option value="fourth">Fourth</option>
                                            <option value="fifth">Fifth</option>
                                            <option value="sixth">Sixth</option>
                                            <option value="seventh">Seventh</option>
                                            <option value="eighth">Eight</option>
                                            <option value="ninth">Ninth</option>
                                            <option value="tenth">Tenth</option>
                                        </select>
                                    </span>

                                    <span ng-switch-default>
                                        <input id="{{attr.name}}" ng-model="attr.value" class="form-control input-sm autocomplete" />
                                    </span>
                                </div>
                            </div>
                        </form>

                        <div ng-show="isUnfinished()" ng-animate="'slide'" class="row">
                            <div class="col-lg-12 text-warning">All required fields are not filled.</div>
                        </div>
                        <div ng-show="!isUnfinished()" ng-animate="'slide'" class="row">
                            <div class="col-lg-12">&nbsp;</div>
                        </div>
                        <pre id="output" onclick="truben.Utils.selectText('output')"><code>{{output()}}</code></pre>
                    </section>
                    
                    
                    
                    
                </div>
            </div>
        </div>

        <div class="modal" id="export-modal" tabindex="-1" role="dialog" aria-labelledby="export-label" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>Export BibTeX</h4>
                        <div class="text-muted">Mark the entries you want to export and hit the Export-button.</div>
                    </div>
                    <div class="modal-body row">
                        <div class="col-xs-4">
                            <form style="height: 460px; overflow: auto;">
                                <div class="checkbox" ng-repeat="entry in listOfEntries">
                                    <label>
                                        <input type="checkbox" ng-model="entry.export"> 
                                        <span>{{ entry.name}}</span>
                                        <span ng-if="entry.key">: {{ entry.key}}</span>
                                    </label>
                                </div>
                            </form>

                            <button ng-click="export()" class="btn btn-sm btn-success pull-right">Export &nbsp; <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                        <pre class="col-xs-8" id="export-all" style="height: 500px; overflow: auto;" onclick="truben.Utils.selectText('export-all')">{{ exportAll}}</pre>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="import-modal" tabindex="-1" role="dialog" aria-labelledby="import-label" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>Import BibTeX</h4>
                    </div>
                    <div class="modal-body row">
                        <textarea ng-model="importText" id="importText" style="width: 100%; height: 400px; padding: 10px"></textarea>
                        <div class="alert alert-info">{{ importMessage()}}</div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm btn-primary" ng-click="import()"  data-dismiss="modal">Import</button>
                        <button type="button" class="btn btn-default btn-sm btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>



        

    </body>

</html>

<script>
    
</script>
    
