<?php
/** Theme Name: visgroup
 *  Theme Core Functions and Codes
 **/
require get_template_directory() . '/functions/menu/default_menu_walker.php';
require get_template_directory() . '/functions/menu/visgroup_nav_walker.php';
require get_template_directory(). '/inc/icon-functions.php';
require_once dirname(__FILE__) . '/default_options.php';
require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';
require get_template_directory() . '/functions/customize/contact-widgets.php';
include get_template_directory() . '/inc/include-kirki.php';
include get_template_directory() . '/inc/class-visgroup-kirki.php';
function visgroup_customizer_config()
{
    $args = array(
        'capability'   => 'edit_theme_options',
        'option_type'  => 'option',
        'option_name'  => 'visgroup_theme_options',
        'compiler'     => array(),
        'width'        => '22.3%',
    );
    return $args;
}

add_filter('kirki/config', 'visgroup_customizer_config');
require get_template_directory() . '/customizer.php';
add_action('after_setup_theme', 'visgroup_theme_setup');
global $visgroup_theme_options;
function visgroup_theme_setup()
{
    global $content_width;
    //content width
    if (!isset($content_width)) {
        $content_width = 704;
    }
    //px
    //supports featured image
    add_theme_support('post-thumbnails');
    load_theme_textdomain('visgroup', get_template_directory() . '/lang');
    // image resize according to image layout
    add_image_size('visgroup_blog_thumb', 280, 270, true);
    add_image_size('visgroup_home_post_thumb', 276, 200, true);
    add_image_size('visgroup_portfolio_thumb', 358, 258, true);
	add_image_size('visgroup_slider', 1349, 530, true);
	add_image_size('visgroup_page_thumb', 346, 332, true);
	add_image_size('visgroup_post_single', 704, 328, true);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
	// This theme uses wp_nav_menu() in Three locations.
	register_nav_menus( array(
		'primary'    => __( 'Primary menu', 'visgroup' ),
		'secondary' => __( 'Topbar Menu', 'visgroup' ),
		'social' => __( 'Social Links Menu', 'visgroup' ),
	) );
    // theme support
    add_editor_style(get_stylesheet_uri());
    $args = array('default-color' => '#ffffff');
    add_theme_support('custom-background', $args);
	$args1 = array(
	'flex-width'    => true,
	'width'         => 1349,
	'flex-height'    => true,
	'height'        => 114,
	'default-image' => '',
	'header-text-color'=>'#656464',
	'header-text' => true,
);
    add_theme_support('custom-header',$args1);
    add_theme_support('automatic-feed-links');
    add_theme_support('title-tag');
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
/*	add_theme_support( 'custom-logo', array(
		'height'      => 60,
		'width'       => 150,
		'flex-width'  => true,
	) );*/
    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    /*
     * Enable support for Post Formats.
     * See https://developer.wordpress.org/themes/functionality/post-formats/
     */
    add_theme_support( 'post-formats', array(
        'image',
        'video',
        'audio',
        'quote',
        'link',
    ) );
     add_theme_support('woocommerce');
	 add_theme_support( 'starter-content', array(
	 
		'posts' => array(
			'home' => array(
				'template'	=> 'home-page.php',
			),
			'about' => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact' => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog' => array(
				'thumbnail' => '{{image-coffee}}',
			)
		),
		
		'options' => array(
			'fortune_theme_options[portfolio_home]'=>1,
			'show_on_front' => 'page',
			'page_on_front' => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),
		'widgets' => array(
			'sidebar-widget' => array(
				'search',
				'text_business_info',
				'text_about',
				'category',
				'tags',
			),

			'footer-widget' => array(
				'text_business_info',
				'text_about',
				'meta',
				'search',
			),
		),

		'nav_menus' => array(
			'primary' => array(
				'name' => __( 'Primary Menu', 'fortune' ),
				'items' => array(
					'page_home',
					'page_about',
					'page_blog',
					'page_contact',
				),
			),
			'secondary' => array(
				'name' => __( 'Top Menu', 'fortune' ),
				'items' => array(
					'page_home',
					'page_about',
					'page_blog',
					'page_contact',
				),
			),
			'social' => array(
				'name' => __( 'Social Links Menu', 'fortune' ),
				'items' => array(
					'link_yelp',
					'link_facebook',
					'link_twitter',
					'link_instagram',
					'link_email',
				),
			),
		),
	) );
}

add_action('wp_enqueue_scripts', 'visgroup_enqueue_style');
function visgroup_enqueue_style()
{	
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style('visgroup', get_stylesheet_uri());
	wp_enqueue_style('elements-styles', get_template_directory_uri() . '/css/elements-styles.css');
	if(get_theme_mod('color_scheme')!=""){
		wp_enqueue_style('site-color-scheme', get_template_directory_uri() . '/css/skins/'.get_theme_mod('color_scheme'));
	}
    wp_enqueue_style('fontawesome', get_template_directory_uri() . '/css/fonts/font-awesome/css/font-awesome.css');
	wp_enqueue_style('owl.carousel', get_template_directory_uri() . '/vendor/owl-carousel/owl.carousel.css');
	wp_enqueue_style('owl.theme', get_template_directory_uri() . '/vendor/owl-carousel/owl.theme.css');
	wp_enqueue_style('ElasticSlider', get_template_directory_uri() . '/vendor/ElasticSlider/css/estyle.css');
    wp_enqueue_style('magnific-popup', get_template_directory_uri() . '/vendor/magnific-popup/magnific-popup.css');
	wp_enqueue_script('modernizr', get_template_directory_uri() . '/vendor/modernizr.js', array('jquery'));
	wp_enqueue_script ('htmlshiv', get_template_directory_uri() . '/vendor/htmlshiv.js');
	wp_script_add_data( 'htmlshiv', 'conditional', 'lt IE 9' );
	wp_enqueue_script ('respond', get_template_directory_uri() . '/vendor/respond.js');
	wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );
	wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css');
	$visgroup_custom_css='.header .logo h1, .header .logo .tagline, .fhmm .navbar-collapse .navbar-nav > li > a{color:#'. esc_attr(get_header_textcolor()).';}';
	wp_add_inline_style( 'visgroup', $visgroup_custom_css );
    if (is_singular()) {
        wp_enqueue_script("comment-reply");
    }
    wp_enqueue_style('Playfair','//fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic');
    wp_enqueue_style('Goudy', '//fonts.googleapis.com/css?family=Goudy+Bookletter+1911&text=&');
	wp_enqueue_style('antom','//fonts.googleapis.com/css?family=Anton|Muli:300,400,400italic,300italic|Oswald');
}

add_action('wp_footer', 'visgroup_enqueue_in_footer');
function visgroup_enqueue_in_footer()
{	$visgroup_theme_options = visgroup_theme_options();
    wp_enqueue_script('jquery-migrate-1.2.1', get_template_directory_uri() . '/vendor/jquery-migrate-1.2.1.js', array('jquery'));
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/vendor/bootstrap.js', array('jquery'));
	wp_enqueue_script('headhesive.js', get_template_directory_uri() . '/vendor/headhesive.js', array('jquery'));
	wp_enqueue_script('jquery.magnific-popup', get_template_directory_uri() . '/vendor/magnific-popup/jquery.magnific-popup.js', array('jquery'));
	if(class_exists('WooCommerce')){
		if(is_shop() || is_cart() || is_product() || is_checkout() || is_product_category()){
			wp_enqueue_script('jquery.dcjqaccordion', get_template_directory_uri() . '/vendor/jquery.dcjqaccordion.js', array('jquery'));
			$dcjq ='  jQuery(".product-categories").dcAccordion({
					saveState: false,
					autoExpand: true,
					showCount: true,
				});
			jQuery(".dcjq-icon").click(function(){
				$(this).toggleClass("less");
			});';
			wp_add_inline_script('jquery.dcjqaccordion',$dcjq);
		}
	}
	wp_enqueue_script('owl.carousel', get_template_directory_uri() . '/vendor/owl-carousel/owl.carousel.js', array('jquery'));
	wp_enqueue_script('ElasticSlider-js', get_template_directory_uri() . '/vendor/ElasticSlider/js/jquery.eislideshow.js');
	wp_enqueue_script('jquery.fitvids', get_template_directory_uri() . '/vendor/jquery.fitvids.js', array('jquery'));	
	wp_enqueue_script('jquery.appear', get_template_directory_uri() . '/vendor/jquery.appear.js', array('jquery'));
	wp_enqueue_script('jquery.easing', get_template_directory_uri() . '/vendor/jquery.easing.1.3.js', array('jquery'));
	wp_enqueue_script('custom', get_template_directory_uri() . '/vendor/custom.js', array('jquery'));
	wp_localize_script('custom','header',array('is_sticky'=>$visgroup_theme_options['headersticky']));

}
// Migrate any existing theme CSS to the core option added in WordPress 4.7.
if ( function_exists( 'wp_get_custom_css' ) && class_exists('Kirki') ) {
	  $fields = Kirki::$fields;
		$fields['visgroup_theme_options[custom_css]']['description']="This field will no longer work. Since WordPress 4.7 has it's own Custom CSS Editor we recommed you to put all your custom CSS in that field.
		";

		$fields['visgroup_theme_options[custom_css]']['tooltip']="This field will no longer work. Since WordPress 4.7 has it's own Custom CSs Editor we recommed you to put all your custom CSS in that field.
		";
		Kirki::$fields = $fields;
}else{
	add_action('wp_head','visgroup_custom_css');
	function visgroup_custom_css(){
		$visgroup_theme_options = visgroup_theme_options();
		if($visgroup_theme_options['custom_css']!=""){?>
			<style type="text/css"><?php echo ($visgroup_theme_options['custom_css']); ?></style>
		<?php }
	}
}

// Read more tag to formatting in blog page
function visgroup_content_more($read_more)
{
    return '<div class=""><a class="main-button" href="' . get_permalink() . '">' . __('Read More', 'visgroup') . '<i class="fa fa-angle-right"></i></a></div>';
}

add_filter('the_content_more_link', 'visgroup_content_more');
// Replaces the excerpt "more" text by a link
function visgroup_excerpt_more($more)
{	return '<footer class="entry-footer">
		<a href="'.esc_url(get_permalink()).'" class="btn btn-default">' . __('Read More', 'visgroup') . '</a>
	</footer>';
}

add_filter('excerpt_more', 'visgroup_excerpt_more');
/*
 * visgroup widget area
 */
add_action('widgets_init', 'visgroup_widget');
function visgroup_widget()
{
    /*sidebar*/
    $visgroup_theme_options = visgroup_theme_options();
    $col                = 12 / (int) $visgroup_theme_options['footer_layout'];
    register_sidebar(array(
        'name'          => __('Sidebar Widget Area', 'visgroup'),
        'id'            => 'sidebar-widget',
        'description'   => __('Sidebar widget area', 'visgroup'),
        'before_widget' => '<div class="widget_categories widget widget__sidebar">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));
    register_sidebar(array(
        'name'          => __('Footer Widget Area', 'visgroup'),
        'id'            => 'footer-widget',
        'description'   => __('Footer widget area', 'visgroup'),
        'before_widget' => '<div class="col-sm-6 col-md-'.$col.'">
								<div class="widget_categories widget widget__footer">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));
}

/* Breadcrumbs  */
function visgroup_breadcrumbs()
{
    $delimiter = "";
    $home      = __('Home', 'visgroup'); // text for the 'Home' link
    $pre_text  = '';
    $before    = '<li>'; // tag before the current crumb
    $after     = '</li>'; // tag after the current crumb
    echo '<ul class="breadcrumb">';
    global $post;
    $homeLink = home_url();
    echo '<li>' . $pre_text . '<a href="' . $homeLink . '">' . $home . '</a>' . $after;
    if (is_category()) {
        global $wp_query;
        $cat_obj   = $wp_query->get_queried_object();
        $thisCat   = $cat_obj->term_id;
        $thisCat   = get_category($thisCat);
        $parentCat = get_category($thisCat->parent);
        if ($thisCat->parent != 0) {
            echo (get_category_parents($parentCat, true, ' ' . $delimiter . '</li> '));
        }

        echo $before .  single_cat_title('', false) . $after;
    } elseif (is_day()) {
        echo '<li><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . get_the_time('Y') . '</a>' . $delimiter . '</li>';
        echo '<li><a href="' . esc_url(get_month_link(get_the_time('Y')), get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter;
        echo $before . get_the_time('d') . '</li>';
    } elseif (is_month()) {
        echo '<li><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . get_the_time('Y') . '</a>' . $delimiter;
        echo $before . get_the_time('F') . '</li>';
    } elseif (is_year()) {
        echo $before . get_the_time('Y') . '</li>';
    } elseif (is_single() && !is_attachment()) {
        if (get_post_type() != 'post') {
            $post_type = get_post_type_object(get_post_type());
            $slug      = $post_type->rewrite;
            echo '<li><a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter;
            echo $before . get_the_title() . '</li>';
        } else {
            $cat = get_the_category();
            $cat = $cat[0];
            echo $before . get_the_title() . '</li>';
        }
    } elseif (!is_single() && !is_page() && get_post_type() && get_post_type() != 'post') {
        $post_type = get_post_type_object(get_post_type());
        echo $before . $post_type->labels->singular_name . $after;
    } elseif (is_attachment()) {
        $parent = get_post($post->post_parent);
        $cat    = get_the_category($parent->ID);
        $cat    = $cat[0];
        echo get_category_parents($cat, true, ' ' . $delimiter . ' ');
        echo '<li><a href="' . esc_url(get_permalink($parent)) . '">' . $parent->post_title . '</a>' . $delimiter;
        echo $before . esc_attr(get_the_title()) . $after;
    } elseif (is_page() && !$post->post_parent) {
        echo $before . esc_attr(get_the_title()) . $after;
    } elseif (is_page() && $post->post_parent) {
        $parent_id   = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
            $page          = get_page($parent_id);
            $breadcrumbs[] = '<li><a href="' . esc_url(get_permalink($page->ID)) . '">' . esc_attr(get_the_title($page->ID)) . '</a></li>';
            $parent_id     = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        foreach ($breadcrumbs as $crumb) {
            echo $crumb . ' ' . $delimiter . ' ';
        }

        echo $before . esc_attr(get_the_title()) . $after;
    } elseif (is_search()) {
        echo $before  .'"'. esc_attr(get_search_query()) . '"' . $after;
    } elseif (is_tag()) {
        echo $before . single_tag_title('', false) . $after;
    } elseif (is_author()) {
        global $author;
        $userdata = get_userdata($author);
        echo $before . $userdata->display_name . $after;
    } elseif (is_404()) {
        echo '<li>' . _e(" Error 404 ", 'visgroup') . '</li>';
    }
    echo '</ul>';
}
/* add a class to avatar */
add_filter('get_avatar','visgroup_change_avatar_css');
function visgroup_change_avatar_css($class) {
$class = str_replace("class='avatar", "class='gravatar ", $class) ;
return $class;
}
/* change class of comment reply link */
add_filter('comment_reply_link', 'visgroup_replace_reply_link_class');
function visgroup_replace_reply_link_class($class){
    $class = str_replace("class='comment-reply-link", "class='btn btn-sm btn-default", $class);
	$class = str_replace(">Reply</a>", "><i class=\"fa fa-reply\"></i>"._e("Reply", 'visgroup')."</a>", $class);
    return $class;
}
function visgroup_comments($comments, $args, $depth)
{
    $GLOBALS['comment'] = $comments;
    extract($args, EXTR_SKIP);
    if ('div' == $args['style']) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <li  <?php comment_class("comment"); ?>>
    <div class="comment-wrapper">
        <div class="comment-author vcard"><?php 
			if ($args['avatar_size'] != 0) {
        		echo get_avatar($comments, $args['avatar_size']);
   			 } ?>
		<h5><?php printf('%s', esc_attr(get_comment_author()));?></h5>
		<span class="says"><?php _e('says:','visgroup'); ?></span>
          <div class="comment-meta"> <a href="#"><?php printf(__('%1$s at %2$s', 'visgroup'), get_comment_date(), get_comment_time());?></a>
		   <?php edit_comment_link(); ?>
		  </div>
        </div><?php
		if ($comments->comment_approved != '0') { ?>
        <div class="comment-reply"> <?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'])));?> </div>
        <div class="comment-body"><?php comment_text();?></div><?php
		}else{
		echo '<p>'.__('Your comment is waiting for moderation.','visgroup').'</p>';
		} ?>
      </div>
<?php
}
/* Blog Pagination */
if (!function_exists('visgroup_pagination')) {
function visgroup_pagination() {
    global $wp_query;
    $big = 999999999; // need an unlikely integer
    $pages = paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
            'prev_next' => false,
            'type'  => 'array',
            'prev_next'   => TRUE,
			'prev_text'    => '&#171;',
			'next_text'    => '&#187;',
        ) );
        if( is_array( $pages ) ) {
            $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
            echo '<div class="text-center"><ul class="pagination-custom list-unstyled list-inline">';
            foreach ( $pages as $page ) {
				$page = str_replace('page-numbers','btn btn-sm btn-default',$page);
                echo "<li>$page</li>";
            }
           echo '</ul></div>';
        }
}
}

/* TGMPA register */
add_action('tgmpa_register', 'visgroup_register_required_plugins');
function visgroup_register_required_plugins()
{
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
        // This is an example of how to include a plugin bundled with a theme.
        array(
            'name'     => 'WooCommerce Grid / List toggle', // The plugin name.
            'slug'     => 'woocommerce-grid-list-toggle', // The plugin slug (typically the folder name).
            'required' => false, // If false, the plugin is only 'recommended' instead of required.
        ),
		
		array(
            'name'     => 'YITH WooCommerce Zoom Magnifier', // The plugin name.
            'slug'     => 'yith-woocommerce-zoom-magnifier', // The plugin slug (typically the folder name).
            'required' => false, // If false, the plugin is only 'recommended' instead of required.
        ),
		
		array(
            'name'     => 'Photo Video Gallery Master', // The plugin name.
            'slug'     => 'photo-video-gallery-master', // The plugin slug (typically the folder name).
            'required' => false, // If false, the plugin is only 'recommended' instead of required.
        ),
		array(
            'name'     => 'Ultimate Gallery Master', // The plugin name.
            'slug'     => 'ultimate-gallery-master', // The plugin slug (typically the folder name).
            'required' => false, // If false, the plugin is only 'recommended' instead of required.
        ),
		array(
            'name'     => 'Social Media Gallery', // The plugin name.
            'slug'     => 'social-media-gallery', // The plugin slug (typically the folder name).
            'required' => false, // If false, the plugin is only 'recommended' instead of required.
        ),
    );
    $config = array(
        'id'           => 'visgroup', // Unique ID for hashing notices for multiple instances of visgroup.
        'default_path' => '', // Default absolute path to bundled plugins.
        'menu'         => 'visgroup-install-plugins', // Menu slug.
        'parent_slug'  => 'themes.php', // Parent menu slug.
        'capability'   => 'edit_theme_options', // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true, // Show admin notices or not.
        'dismissable'  => true, // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '', // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false, // Automatically activate plugins after installation or not.
        'message'      => '', // Message to output right before the plugins table.
    );
    visgroup($plugins, $config);
}

/* Woocommerce supoport */
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_before_main_content', 'visgroup_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'visgroup_theme_wrapper_end', 10);
function visgroup_theme_wrapper_start()
{?>
    <section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php $post_type = get_post_type_object(get_post_type());
        echo $post_type->labels->singular_name ; ?></h1>
            </div>
            <div class="col-md-6">
                <?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section><?php
    echo '<section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">';
}
function visgroup_theme_wrapper_end()
{?>
</div><?php get_sidebar(); ?></div></div>
        </section>
<?php }
add_filter( 'woocommerce_show_page_title' , 'visgroup_hide_page_title' );
/**
 * visgroup_hide_page_title
 *
 * Removes the "shop" title on the main shop page
*/
function visgroup_hide_page_title() {    
    return false;   
}
/**
 * Removes breadcrumbs
*/
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
/** Remove Showing results functionality site-wide */
function woocommerce_result_count() {
        return;
}

@ini_set( 'upload_max_size' , '64M' );

/**
 * Register `group` post type
 */
function group_post_type() {
   
   // Labels
	$labels = array(
		'name' => _x("Groups", "post type general name"),
		'singular_name' => _x("Group", "post type singular name"),
		'menu_name' => 'Groups',
		'add_new' => _x("Add New", "group"),
		'add_new_item' => __("Add New Group"),
		'edit_item' => __("Edit Group"),
		'new_item' => __("New Group"),
		'view_item' => __("View Group"),
		'search_items' => __("Search Groups"),
		'not_found' =>  __("No Groups Found"),
		'not_found_in_trash' => __("No Groups Found in Trash"),
		'parent_item_colon' => ''
	);
	
	// Register post type
	register_post_type('groups' , array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => false,
		'menu_icon' => 'dashicons-admin-users',
		'rewrite' => true,
		'supports' => array('title', 'editor', 'thumbnail')
	) );
}
add_action( 'init', 'group_post_type', 0 );


/**
 * Register `team` post type
 */
function team_post_type() {
   
   // Labels
	$labels = array(
		'name' => _x("Team", "post type general name"),
		'singular_name' => _x("Team", "post type singular name"),
		'menu_name' => 'People',
		'add_new' => _x("Add New", "team"),
		'add_new_item' => __("Add New Profile"),
		'edit_item' => __("Edit Profile"),
		'new_item' => __("New Profile"),
		'view_item' => __("View Profile"),
		'search_items' => __("Search Profiles"),
		'not_found' =>  __("No Profiles Found"),
		'not_found_in_trash' => __("No Profiles Found in Trash"),
		'parent_item_colon' => ''
	);
	
	// Register post type
	register_post_type('team' , array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => false,
		'menu_icon' => 'dashicons-admin-users',
		'rewrite' => true,
		'supports' => array('title', 'editor', 'thumbnail')
	) );
}
add_action( 'init', 'team_post_type', 0 );
    
    /**
     * Register `project` post type
     */
function project_post_type() {
        
        // Labels
    $labels = array(
                        'name' => _x("Projects", "post type general name"),
                        'singular_name' => _x("Project", "post type singular name"),
                        'menu_name' => 'Projects',
                        'add_new' => _x("Add New", "project"),
                        'add_new_item' => __("Add New Project"),
                        'edit_item' => __("Edit Project"),
                        'new_item' => __("New Project"),
                        'view_item' => __("View Project"),
                        'search_items' => __("Search Projects"),
                        'not_found' =>  __("No Projects Found"),
                        'not_found_in_trash' => __("No Projects Found in Trash"),
                        'parent_item_colon' => ''
                        );
        
        // Register post type
    register_post_type('projects' , array(
                                          'labels' => $labels,
                                          'public' => true,
                                          'has_archive' => false,
                                          'menu_icon' => 'dashicons-admin-users',
                                          'rewrite' => true,
                                          'supports' => array('title', 'editor', 'thumbnail')
                                          ) );
}
add_action( 'init', 'project_post_type', 0 );
    
function course_post_type() {

    // Labels
    $labels = array(
                    'name' => _x("Courses", "post type general name"),
                    'singular_name' => _x("Course", "post type singular name"),
                    'menu_name' => 'Courses',
                    'add_new' => _x("Add New", "course"),
                    'add_new_item' => __("Add New Course"),
                    'edit_item' => __("Edit Course"),
                    'new_item' => __("New Course"),
                    'view_item' => __("View Course"),
                    'search_items' => __("Search Courses"),
                    'not_found' =>  __("No Courses Found"),
                    'not_found_in_trash' => __("No Courses Found in Trash"),
                    'parent_item_colon' => ''
                    );

    // Register post type
    register_post_type('courses' , array(
                                         'labels' => $labels,
                                         'public' => true,
                                         'has_archive' => false,
                                         'menu_icon' => 'dashicons-admin-users',
                                         'rewrite' => true,
                                         'supports' => array('title', 'editor', 'thumbnail')
                                         ) );
}
add_action( 'init', 'course_post_type', 0 );
    
function event_post_type() {

    // Labels
    $labels = array(
                    'name' => _x("Events", "post type general name"),
                    'singular_name' => _x("Event", "post type singular name"),
                    'menu_name' => 'Events',
                    'add_new' => _x("Add New", "event"),
                    'add_new_item' => __("Add New Event"),
                    'edit_item' => __("Edit Event"),
                    'new_item' => __("New Event"),
                    'view_item' => __("View Event"),
                    'search_items' => __("Search Events"),
                    'not_found' =>  __("No Profiles Found"),
                    'not_found_in_trash' => __("No Profiles Found in Trash"),
                    'parent_item_colon' => ''
                    );

    // Register post type
    register_post_type('events' , array(
                                         'labels' => $labels,
                                         'public' => true,
                                         'has_archive' => false,
                                         'menu_icon' => 'dashicons-admin-users',
                                         'rewrite' => true,
                                         'supports' => array('title', 'editor', 'thumbnail')
                                         ) );
}
add_action( 'init', 'event_post_type', 0 );


function add_query_vars_filter( $vars ){
    $vars[] = "pubkey";
    return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );
    

function custom_rewrite_publication() {
    add_rewrite_rule('^publications/([^/]*)/?', 'index.php?page_id=9&pubkey=$matches[1]', 'top');
    add_rewrite_rule('^team/.*/publications\/([^/]*)\/?', 'index.php?page_id=9&pubkey=$matches[1]', 'top');
    #add_rewrite_rule('^publications/([^/]*)/?', 'index.php?page_id=173&pubkey=$matches[1]', 'top');
    #add_rewrite_rule('^team/.*/publications\/([^/]*)\/?', 'index.php?page_id=173&pubkey=$matches[1]', 'top');
    
}
add_action('init', 'custom_rewrite_publication');

function custom_rewrite_basic() {
        
}
add_action('init', 'custom_rewrite_basic');

add_filter( 'get_the_archive_title', function ( $title ) {

    if( is_category() ) {

        $title = single_cat_title( '', false );

    }

    return $title;

});
?>
