<?php
/**
 * Template Name: Publications Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<section class="content">
	
	<?php 

        ini_set('display_errors',1);
        ini_set('display_startup_errors',1);
        error_reporting(-1);

        $key = get_query_var( 'publication', '');
                    
        if ($key=='')
        {
            get_template_part('inc/page-title');
            echo papercite_cb("[bibtex template=visgroup-list key_format=cite]");            
        }
        else
        {
            echo papercite_cb("[bibtex template=visgroup-page key=".$key."]");
?>
            <div class="pad group">
<?php 
            query_posts('tag='.$key);

		    // Start the Loop.
		    while ( have_posts() ) : the_post();

			    // Include the page content template.
			    get_template_part( 'content', 'page' );


			    // If comments are open or we have at least one comment, load up the comment template.
			    if ( comments_open() || get_comments_number() ) {
				    comments_template();
			    }
		    endwhile;
        }
?>
        </div>
	
</section><!--/.content-->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
