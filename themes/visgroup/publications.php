<?php
/**
 * Template Name: Publications
 */

get_header();
the_post();

// Get 'team' posts
global $post;
    $team_posts = get_posts( array(
                                   'post_type' => 'team',
                                   'posts_per_page' => -1,     // Unlimited posts
                                   'orderby' => 'title', // Order alphabetically by name
    ) );

$compare_code = 
            'setup_postdata($a);'
            . '$nameA = explode(" ", get_the_title($a));'
            . '$lastNameA= $nameA[count($nameA)-1];'
            . 'setup_postdata($b);'
            . '$nameB = explode(" ", get_the_title($b));'
            . '$lastNameB = $nameB[count($nameB)-1];'
            . 'return strnatcasecmp($lastNameA, $lastNameB);';

usort($team_posts, create_function('$a, $b',$compare_code));
$result = "";

foreach ( $team_posts as $post ):
    global $result;
    setup_postdata( $post );
    $name =$post->post_title;
    $lastName = explode(" ", $name);
    
    if ($result == ""){
        $result = $lastName[count($lastName)-1];
    }
    else{
        $result =  $result . "|" . $lastName[count($lastName)-1]; ;
    }
endforeach;


wp_reset_postdata();

function getVideoFile(&$inputstring)
{   
    $matches = array();
    echo "tesT";
    //$res = preq_match("VIDEO\:([^\n]*)?",$inputstring,$matches);
    $start_pos = strpos($inputstring,"VIDEO");
    
    echo $res;
    return $res;
    
}
    
?>

<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                    <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-md-6">
                    <?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Content -->
<section class="page-content">
  <div class="container">
  	<div class="row">
            <div class="content col-md-8">
                <?php $key = get_query_var( 'pubkey', '');

                if ($key!='')
                {
                    get_template_part('inc/page-title');
                    
                    $paperciteoutput = papercite_cb("[bibtex file=publications-full.bib template=visgroup-page key=".$key."]");
     
                    $fixedoutput = str_replace("http:", "" , $paperciteoutput);
                    echo $fixedoutput;
                   
                }
                else
                {
                    global $result;
                    $papercitecall = "[bibfilter file=publications-full.bib group=year group_order=desc filter:affiliation=/^$/ author=" . $result . " allow=incollection,inproceedings,book,phdthesis,article,misc  sortbyauthor=1]";
                    $paperciteoutput = papercite_cb($papercitecall);
                    
                    $fixedoutput = str_replace("WP_ROOTFOLDER",  get_site_url(), $paperciteoutput);
                   
                    echo $fixedoutput;
                }
                
                ?>
            
           
            
            </div>
            <?php
            if ($key!=''):
            ?>    
            <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                    <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                        		
                        <?php 
                        $matches = array();
                        #preg_match('/project_id\">(.*)<\/div>/', $paperciteoutput, $matches);
                        preg_match('/projectid(.*)projectid/', $paperciteoutput, $matches);
                        
                        if(sizeof($matches) == 2 && $matches[1] != '')
                        {
                            echo '<h3 class="widget-title">Related papers</h3>';
                            $projects = explode(',',$matches[1]);
                            $papercitecall = "[bibtex file=publications-full.bib  template=visgroup-list-small group=year group_order=desc filter:project={" . $projects[0] . "}]";
                            $paperciteoutput = papercite_cb($papercitecall);
                            $fixedoutput = str_replace("WP_ROOTFOLDER",  get_site_url(), $paperciteoutput);
                            
                            echo $fixedoutput;
                        }
                        
                                    
                        ?>
                    </div>
                
                
            </aside>
            <?php
            else:
                ?>
            <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                    <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                        <h3 class="widget-title">Shortcuts</h3>		
                        <ul id="scroll_links">
                           
                        </ul>
                    </div>
                
                
            </aside>
            <?php
            endif;             
            ?>
            
           
        
	</div>
  </div>
</section>
<script>
    var placeHolder = document.getElementById('scroll_links');
    if(placeHolder) {
        var years = document.getElementsByClassName('papercite');

        for (i = 0; i < years.length; i++)
        {
            var el = years[i];
            var newYear = document.createElement('li');
            newYear.innerHTML = "<a href=#"+el.id+">"+el.id+"</a>";
            placeHolder.appendChild(newYear);
        }
    }

</script>
<!-- Page Content / End -->
<?php get_footer(); ?>
