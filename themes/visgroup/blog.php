<?php /* Template Name: Blog */
get_header();
?>
<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-md-6">
<?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Heading / End -->
<!-- Page Content -->
<section class="page-content">
    <div class="container">

        <div class="row">
            <div class="content col-md-8"><?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array('post_type' => 'post', 'paged' => $paged);
                $wp_query = new WP_Query($args);
                while ($wp_query->have_posts()):
                    $wp_query->the_post();
                    global $read_more;
                    $read_more = 0;
                    get_template_part('blog', 'content');
                endwhile;
                visgroup_pagination();
                ?>
            </div>
             <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                    <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                        
                        <h3 class="widget-title">Job Offers</h3>		
                        <ul id="scroll_links">
                            <?php
                            
                            $job_posts = get_posts(array(
                               'post_type' => 'projects', 
                                'posts_per_page' => -1, // Unlimited posts
                                'orderby' => 'title', // Order alphabetically by name
                                'order'=>'ASC'
                                ));
                            foreach ($job_posts as $jobs  ):
                                
                                echo "<li><a href=". get_permalink($jobs->ID) . ">" . $jobs->post_title."</a></li>";
                            endforeach;
                            ?>
                        </ul>
                    </div>
                
                
            </aside>
        
        </div>
    </div>
</section>
<?php get_footer(); ?>
