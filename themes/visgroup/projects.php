<?php
/**
 * Template Name: projects
 */
get_header();
the_post();

// Get 'team' posts
$team_posts = get_posts(array(
    'post_type' => 'projects',
    'posts_per_page' => -1, // Unlimited posts
    'orderby' => 'title', // Order alphabetically by name
    'order' => 'ASC',
        ));
?>

<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-md-6">
                <?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Content -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="content col-md-8">
                <div class="content col-md-12">
                    
                    <?php
                    
                       
                    foreach ($team_posts as $post):
                        present_post($post, false);


                     endforeach; 
                    ?>

                </div>

                <h1>Completed projects</h1>

                <div class="content col-md-12">

                    <?php
                    foreach ($team_posts as $post):
                        
                        present_post($post, true);
                        
                    endforeach;
                    
                    ?>
                </div>

            </div>    
            <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                   <hr class="visible-sm visible-xs lg">
                    <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                        
                        <h3 class="widget-title"></h3>		
                        <ul id="scroll_links">
                            <?php
                            
                            $job_posts = get_posts(array(
                               'post_type' => 'projects', 
                                'posts_per_page' => -1, // Unlimited posts
                                'orderby' => 'title', // Order alphabetically by name
                                'order'=>'ASC'
                                ));
                            foreach ($job_posts as $jobs  ):
                                
                                echo "<li><a href=". get_permalink($jobs->ID) . ">" . $jobs->post_title."</a></li>";
                            endforeach;
                            ?>
                        </ul>
                    </div>
                
                
            </aside>
        </div>
    </div>
</section>
<!-- Page Content / End -->
<?php get_footer();     
function present_post($post, $skipongoing)
{
    setup_postdata($post);
    if (get_field('project_ongoing') == $skipongoing) {
        wp_reset_postdata(); 
    
        return;
    }
    $thumb_src = null;
    if (has_post_thumbnail($post->ID)) {
        $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'team-thumb');
        $thumb_src = $src[0];
    }
    ?>  

    <div class="row team-entry"  >
        <div class="team-image" >
            <?php if (has_post_thumbnail()) : ?>

                <?php echo get_the_post_thumbnail($post->ID,'thumbnail');?>
            <?php else: ?>

                <img  src= "<?php bloginfo('template_directory'); ?>/images/noimage.png"/>

            <?php endif; ?>
        </div>
        <div class="team-info">

            <h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

            <div class="team-details">
                <ul class="fa-ul">         
                    <?php if ($funding = get_field('project_funding')): ?>
                    <li>Funded by: <?php echo $funding; ?></li>
                    <?php endif; ?>
                    <br>
                     <?php if ($contacts = get_field('project_contact')): 
                        
                            echo 'Contact: ';
                            echo '<ul>';
                            foreach ($contacts as $contact):
                                
                                echo "<li><a href=". $contact->post_permalink . ">" . $contact->post_title . "</a></li>";

                                
                            endforeach;
                            echo '</ul>';
                            ?>

                       
                   
                    <?php endif; ?>
                </ul>
            </div>
        </div>

    </div>
<?php
    wp_reset_postdata(); 
}
?>