<?php get_header();
the_post();
?>
<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-md-6">
<?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Content -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="content col-md-8">
                <!--			<div class="title-accent">
                                          <h3><?php the_title(); ?></h3>
                                        </div>-->
                    <?php if (has_post_thumbnail()) {
                        $img_class = array('class' => 'img_responsive'); ?>
                    <figure class="alignleft"> <?php the_post_thumbnail('visgroup_page_thumb', $img_class); ?> </figure>
            <?php } ?>
            <?php the_content(); ?>
            </div>
            <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                    <h3 class="widget-title">Job Offers</h3>
                    <ul id="scroll_links">
                    <?php

                        $job_posts = get_posts(array(
                                                     'post_type' => 'post',
                                                     'category_name' => 'jobs',
                                                     'posts_per_page' => -1, // Unlimited posts
                                                     'orderby' => 'title', // Order alphabetically by name
                                                     'order'=>'ASC'
                                                     ));
                        foreach ($job_posts as $jobs  ):

                        echo "<li><a href=". get_permalink($jobs->ID) . ">" . $jobs->post_title."</a></li>";
                        endforeach;
                        ?>
                    </ul>
                </div>


            </aside>        
        </div>
    </div>
</section>
<!-- Page Content / End -->
<?php get_footer(); ?>
