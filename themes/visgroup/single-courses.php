<?php get_header();
the_post();
?>


<?php
function displayMastersPapercite(){
    
    $papercitecall = "[bibtex file=publications-full.bib allow=masterthesis]";
    $paperciteoutput = papercite_cb($papercitecall);
    $fixedoutput = str_replace("WP_ROOTFOLDER",  get_site_url(), $paperciteoutput);
    echo $fixedoutput;
}
add_shortcode("displayMasters", 'displayMastersPapercite');    
?>
<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-md-6">
<?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Content -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="content col-md-8">
                 <div class="content col-md-12">
                    <div class="alignleft col-md-4">
                        <?php if (has_post_thumbnail()):?>
                            <?php the_post_thumbnail(); ?> 
                        <?php endif; ?>
                    </div>
                    <div class="team-info col-md-8">
                    
                    <h2><?php the_field('team_position'); ?></h2>
                    <p><?php the_field('course_description'); ?></p>

                    
                    <div class="team-details">
                      <ul class="fa-ul">
                      <?php if ($code = get_field("course_code")): ?>
                        <li><i class="fa fa-link fa-li"></i><a href="http://www.uib.no/en/course/<?php echo $code ?>" title="<?php echo $code; ?>@uib.no"><?php echo $code  ?>@uib.no</a></li>                             
                      <?php endif; ?>
                      
                      <?php if ($teachers = get_field('course_teachers')): ?>
                        
                            <?php
                            foreach ($teachers as $teacher): 
                            ?>                                                            
                                <li><i class="fa fa-user fa-li"></i><a href="<?php echo get_permalink($teacher->ID); ?>" title="<?php echo get_the_title($teacher->ID); ?>"><?php echo get_the_title($teacher->ID); ?></a></li>
                            <?php
                            endforeach; 
                            ?>
                      <?php endif; ?>                             
                      </ul>
                    </div>
                    
                    </div>
                    
                    <div>                   
                      <?php the_content(); ?>
                    </div>
                       <?php if ($category = get_field("course_category")):
                           echo "<h3>Available Projects</h3>";
                           $project_posts = get_posts(array(    
                               'post_type' => 'post', 
                               'category_name' => $category,
                                'posts_per_page' => -1, // Unlimited posts
                                'orderby' => 'title', // Order alphabetically by name
                                'order'=>'ASC'
                                ));
                            foreach ($project_posts as $project  ):
                                
                                echo "<a href=". get_permalink($project->ID) . ">" . $project->post_title."</a><br>";
                            endforeach;
                            
                        endif;
                       ?>
                </div>

            </div>
             <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                    <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                        
                        <h3 class="widget-title"></h3>		
                        <ul id="scroll_links">
                            <?php
                            
                            $job_posts = get_posts(array(
                               'post_type' => 'courses', 
                                'posts_per_page' => -1, // Unlimited posts
                                'orderby' => 'title', // Order alphabetically by name
                                'order'=>'ASC'
                                ));
                            foreach ($job_posts as $jobs  ):
                                
                                echo "<li><a href=". get_permalink($jobs->ID) . ">" . $jobs->post_title."</a></li>";
                            endforeach;
                            ?>
                        </ul>
                    </div>
                
                
            </aside>    
        </div>
    </div>
</section>

<!-- Page Content / End -->
<?php get_footer(); ?>
