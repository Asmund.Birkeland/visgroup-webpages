<?php
get_header();
the_post();
?>
<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-md-6">
                <?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Content -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="content col-md-8">
                <div class="content col-md-12">
                        
                <?php
                if (has_post_thumbnail()):?>
                    <div class="alignleft col-md-4">
                        <?php the_post_thumbnail(); ?> 
                    </div>
                <?php endif; ?>
                    
                <div class="team-info col-md-8">

                    <div class="team-details">
                        <ul class="fa-ul">
                                <?php if ($membemrs = get_field('project_contact')): ?>
                                <p>Contact:
                                    <?php
                                    foreach ($membemrs as $post):
                                        setup_postdata($post);
                                        ?>

                                        <a href=<?php the_permalink(); ?> title="<?php the_title(); ?>">
                                        <?php the_title(); ?>
                                        </a>
                                    <?php endforeach; ?>
                                </p>
                                <?php
                                wp_reset_postdata();
                            endif;
                            ?>

                                <?php if ($membemrs = get_field('project_members')): ?>
                                <p>Members:
                                    <?php
                                    foreach ($membemrs as $post):
                                        setup_postdata($post);
                                        ?>

                                        <a href=<?php the_permalink(); ?> title="<?php the_title(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                        <br>
                                <?php endforeach; ?>
                                </p>
                                <?php
                                wp_reset_postdata();
                            endif;
                            ?>

                        </ul>
                    </div>
                </div>
                <div  align="justify">
                    <p>
                        <?php the_content(); ?>
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                <h1>Publications</h1>
                <div >
                    <?php


                    if($name = get_field('project_shorttitle')){
                        $papercitecall = "[bibtex file=publications-full.bib  group=year group_order=desc filter:project={" . $name . "}]";

                        $paperciteoutput = papercite_cb($papercitecall);
                        $fixedoutput = str_replace("WP_ROOTFOLDER", get_site_url(), $paperciteoutput);
                        echo $fixedoutput;
                    }
                    ?>
                </div>
            </div>
            </div>
             <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                    <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                        
                        <h3 class="widget-title">Projects</h3>		
                        <ul id="scroll_links">
                            <?php
                            
                            $job_posts = get_posts(array(
                               'post_type' => 'projects', 
                                'posts_per_page' => -1, // Unlimited posts
                                'orderby' => 'title', // Order alphabetically by name
                                'order'=>'ASC'
                                ));
                            foreach ($job_posts as $jobs  ):
                                
                                echo "<li><a href=". get_permalink($jobs->ID) . ">" . $jobs->post_title."</a></li>";
                            endforeach;
                            ?>
                        </ul>
                    </div>
                
                
            </aside>
        </div>
    </div>
</section>
<!-- Page Content / End -->
<?php get_footer(); ?>

