<?php
/**
 * Template Name: courses
 */
get_header();
the_post();

// Get 'team' posts
$team_posts = get_posts(array(
    'post_type' => 'courses',
    'posts_per_page' => -1, // Unlimited posts
    'orderby' => 'title', // Order alphabetically by name
    'order'=>'ASC'
        ));

$taught = array("Fall", "Spring", "All year");
global $teamPost;
?>


<style> 

    tr { display: block; float: left; }
    th, td { display: block; }
</style>

<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-md-6">
                <?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Content -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="content col-md-8">
                <?php
                if (has_post_thumbnail()) {
                    $img_class = array('class' => 'img_responsive');
                    ?>
                    <figure class="alignleft"> <?php the_post_thumbnail('visgroup_page_thumb', $img_class); ?> </figure>
                <?php } ?>
                <?php #the_content(); ?>
            </div>

             <div class="col-md-8">
                 
                <?php if ($team_posts):?>
                    <?php foreach ($taught as $semester): ?>
                        <div class="col-md-12 col-lg-12 col-sm-12 " align="left">
                            <h1><?php echo $semester?></h1>
                        </div>

                        <div class="content col-md-12">

                            <?php
                            foreach ($team_posts as $post):
                                setup_postdata($post);
                                ?>

                                <?php
                                global $semester;
                                if (get_field('course_taught') == $semester):
                                    // Resize and CDNize thumbnails using Automattic Photon service
                                    $thumb_src = null;
                                    if (has_post_thumbnail($post->ID)) {
                                        $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'team-thumb');
                                        $thumb_src = $src[0];
                                    }
                                    ?>

                                    
                                        <div class="row team-entry"  >
                                            <div class="team-image">
                                                <?php if (has_post_thumbnail()) { ?>
<!--                                                    <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                                                        <img  src=<?php echo $thumb_src ?> </img>
                                                    </a>-->
                                                    <?php echo get_the_post_thumbnail($post->ID,'thumbnail');?>
                                                <?php } ?>
                                            </div>

                                            <div class="team-info">

                                                <h3>
                                                    <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                                                    <?php the_title(); ?>
                                                    </a>
                                                </h3>
                                                
                                                <div class="team-details">
                                                <?php if ($desc = get_field('course_description')): ?>
                                                    <p>
                                                        <?php echo $desc; ?>
                                                    </p>
                                                <?php endif; ?>
                                                
                                                  <ul class="fa-ul">
                                                  <?php if ($code = get_field("course_code")): ?>
                                                    <li><i class="fa fa-link fa-li"></i><a href="http://www.uib.no/en/course/<?php echo $code ?>" title="<?php echo $code; ?>@uib.no"><?php echo $code  ?>@uib.no</a></li>                             
                                                  <?php endif; ?>
                                                  
                                                  <?php if ($teachers = get_field('course_teachers')): ?>
                                                    
                                                        <?php
                                                        foreach ($teachers as $teacher): 
                                                        ?>                                                            
                                                            <li><i class="fa fa-user fa-li"></i><a href="<?php echo get_permalink($teacher->ID); ?>" title="<?php echo get_the_title($teacher->ID); ?>"><?php echo get_the_title($teacher->ID); ?></a></li>
                                                        <?php
                                                        endforeach; 
                                                        ?>
                                                  <?php endif; ?>                             
                                                  </ul>
                                            </div>                    
                                        </div>    
                                        </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                    <?php endforeach; ?>                  
                <?php endif; ?>
                
            
            </div>
            <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                    <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                        
                        <h3 class="widget-title">Available Student Projects</h3>		
                        <ul id="scroll_links">
                            <?php
                            
                            $project_posts = get_posts(array(    
                               'post_type' => 'post', 
                               'category_name' => 'student-projects',
                                'posts_per_page' => -1, // Unlimited posts
                                'orderby' => 'title', // Order alphabetically by name
                                'order'=>'ASC'
                                ));
                            foreach ($project_posts as $project  ):
                                
                                echo "<li><a href=". get_permalink($project->ID) . ">" . $project->post_title."</a></li>";
                            endforeach;
                            ?>
                        </ul>
                        <h3 class="widget-title">Completed Student Projects</h3>		
                        <ul id="scroll_links">
                            <?php
                            
                            $project_posts = get_posts(array(    
                               'post_type' => 'post', 
                               'category_name' => 'Completed student projects',
                                'posts_per_page' => -1, // Unlimited posts
                                'orderby' => 'title', // Order alphabetically by name
                                'order'=>'ASC'
                                ));
                            foreach ($project_posts as $project  ):
                                
                                echo "<li><a href=". get_permalink($project->ID) . ">" . $project->post_title."</a></li>";
                            endforeach;
                            ?>
                        </ul>
                    </div>
                
                
            </aside>
        </div>
    </div>
</section>


<!-- Page Content / End -->
<?php get_footer(); ?>
