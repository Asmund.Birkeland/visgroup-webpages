<?php
get_header();
the_post();
?>
<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-md-6">
                <?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Content -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="content col-md-8">
                <div class="content col-md-12">
                    
                    <?php
                    if (has_post_thumbnail()) {
                        
                        ?>
                         <div class="alignleft col-md-4">
                            <?php the_post_thumbnail(); ?> 
                         </div>
                        
                    <?php } ?>
                    
                    <div class="team-info col-md-8">

                        <h2><?php the_field('team_position'); ?></h2>
                        <p><?php the_field('team_topics'); ?></p>

                        <?php

                        $head_args = array(
                            'post_type'		=> 'Groups',
                            'meta_query'		=> array(
                                array(
                                    'key' => 'group_head',
                                    'value' => get_the_ID(),
                                    )
                                )
                        );

                        $head_query = new WP_Query( $head_args );
                        
                        if ( $head_query->have_posts())
                        {
                            while( $head_query->have_posts() )
                            {
                                $head_query->the_post();
                                echo "<p><i class='fa fa-users'></i>&nbsp;Head <a href=". get_permalink($head_query->ID) . ">" . get_the_title($head_query->ID) . "</a></p>";
                            }
                        }
                        else
                        {
                            $group_args = array(
                                'post_type'		=> 'Groups',
                                'meta_query'		=> array(
                                    array(
                                        'key' => 'group_members',
                                        'value' => '"' . get_the_ID() . '"',
                                        'compare' => 'LIKE'
                                        )
                                    )
                            );

                            $group_query = new WP_Query( $group_args );

                            while( $group_query->have_posts() )
                            {
                                $group_query->the_post();
                                echo "<p><i class='fa fa-users'></i>&nbsp;<a href=". get_permalink($group_query->ID) . ">" . get_the_title($group_query->ID) . "</a></p>";
                            }
                        }
                        wp_reset_postdata();
                        ?>
                        
                        <div class="team-details">
                            <ul class="fa-ul">         
                                <?php if ($email = get_field('team_email')): ?>
                                    <li><i class="fa fa-envelope fa-li"></i><a href="mailto:<?php echo antispambot($email); ?>"><?php echo antispambot($email); ?></a></li>
                                <?php endif; ?>

                                <?php if ($phone = get_field('team_phone')): ?>
                                    <li><i class="fa fa-phone fa-li"></i><a href="tel:+47 555 84 <?php echo $phone; ?>">+47 555 84 <?php echo $phone; ?></a></li>
                                <?php endif; ?>

                                    <?php if ($office = get_field('team_office')): ?>
                                    <li><i class="fa fa-home fa-li"></i>
                                        <?php if ($mazemapPOI = get_field('team_mazemap_poi')): ?>
                                            <a href="https://use.mazemap.com/?v=1&sharepoitype=poi&campusid=34&left=5.3311&right=5.3321&top=60.3814&bottom=60.3811&sharepoi=<?php echo $mazemapPOI; ?>">HiB, room <?php echo $office; ?></a>
                                        <?php else: ?>
                                            HiB, room <?php echo $office; ?>
                                    <?php endif; ?>
                                    </li>
                                <?php endif; ?>

                                    <?php if ($supervisors = get_field('team_advisor')): ?>
                                    <p>Supervised by:
                                        <?php
                                        foreach ($supervisors as $post):
                                            setup_postdata($post);
                                            ?>

                                            <a href=<?php the_permalink(); ?> title="<?php the_title(); ?>">
                                            <?php the_title(); ?>
                                            </a>
                                    <?php endforeach; ?>
                                    </p>
                                    <?php
                                    wp_reset_postdata();
                                endif;
                                ?>

                            </ul>
                        </div>
                    </div>
                    <div >  
                        <p align="justify">
                           
                           
                        <?php the_field('team_biography'); ?>
                        </p>
                        <?php the_content(); ?>
                    </div>

                    <div class="col-md-12" >
                        <h1>Publications</h1>
                        <?php
                        $name = get_the_title();
                        #echo $name;
                        $lastName = explode(" ", $name, 2);
                        #echo $lastName[1];

                        $papercitecall = "[bibtex file=publications-full.bib  group=year group_order=desc author=" . $lastName[1] . "]";

                        $paperciteoutput = papercite_cb($papercitecall);
                        $fixedoutput = str_replace("WP_ROOTFOLDER", get_site_url(), $paperciteoutput);
                        echo $fixedoutput;
                        ?>
                    </div>
                </div>
            </div>
            <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                    <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                        
                        <h3 class="widget-title">Job Offers</h3>		
                        <ul id="scroll_links">
                            <?php
                            
                            $job_posts = get_posts(array(
                               'post_type' => 'post', 
                               'category_name' => 'jobs',
                                'posts_per_page' => -1, // Unlimited posts
                                'orderby' => 'title', // Order alphabetically by name
                                'order'=>'ASC'
                                ));
                            foreach ($job_posts as $jobs  ):
                                
                                echo "<li><a href=". get_permalink($jobs->ID) . ">" . $jobs->post_title."</a></li>";
                            endforeach;
                            ?>
                        </ul>
                    </div>
                
                
            </aside>
        </div>
    </div>
</section>
<!-- Page Content / End -->
<?php get_footer(); ?>
