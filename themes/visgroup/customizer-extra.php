<?php
/* Add Customizer Panel */
$visgroup_theme_options = visgroup_theme_options();
Kirki::add_config('visgroup_theme', array(
    'capability'  => 'edit_theme_options',
    'option_type' => 'option',
    'option_name' => 'visgroup_theme_options',
));

Kirki::add_field('visgroup_theme', array(
    'settings'          => 'header_topbar_bg_color',
    'label'             => __('Header Top Bar Background Color', 'visgroup'),
    'description'       => __('Change Top bar Background Color', 'visgroup'),
    'section'           => 'colors',
    'type'              => 'color',
    'priority'          => 9,
    'default'           => '#eae6e2',
    'sanitize_callback' => 'visgroup_sanitize_color',
    'output'            => array(
        array(
            'element'  => '.header-top',
            'property' => 'background',
        ),
    ),

));

Kirki::add_field('visgroup_theme', array(
    'settings'          => 'header_topbar_color',
    'label'             => __('Header Top Bar Color', 'visgroup'),
    'description'       => __('Change Top bar Font Color', 'visgroup'),
    'section'           => 'colors',
    'type'              => 'color',
    'priority'          => 9,
    'default'           => '#333333',
    'sanitize_callback' => 'visgroup_sanitize_color',
    'output'            => array(
        array(
            'element'  => '.header-top, .header-top ul li a, .header-top-right a',
            'property' => 'color',
        ),
    ),
));

Kirki::add_field('visgroup_theme', array(
    'settings'          => 'header_background_color',
    'label'             => __('Header Background Color', 'visgroup'),
    'description'       => __('Change Header Background Color', 'visgroup'),
    'section'           => 'colors',
    'type'              => 'color',
    'priority'          => 9,
    'default'           => '#eae6e2',
    'sanitize_callback' => 'visgroup_sanitize_color',
    'output'            => array(
        array(
            'element'  => '.header-main',
            'property' => 'background',
        ),
    ),
));
Kirki::add_field( 'visgroup_theme', array(
    'type'        => 'spacing',
    'settings'    => 'slider_content_spacing',
    'label'       => __( 'Slider Content Margin Right', 'visgroup' ),
    'section'     => 'slider_sec',
    'default'     => array(
        'right'  => '0%',
    ),
    'output'=>array(
        array(
            'element'  => '.ei-title',
            )),
    'priority'    => 10,
) );
/* Slider content animation effect */
Kirki::add_field( 'visgroup_theme', array(
    'type'        => 'select',
    'settings'    => 'slider_easing_effect',
    'label'       => __( 'Slider Animation Effects', 'visgroup' ),
    'description'=>sprintf('<a href="http://easings.net/">%s</a>',__('Click here to see how each easing effects works.','visgroup')),
    'section'     => 'slider_sec',
    'default'     => 'easeOutExpo',
    'choices'=>array(
        "easeInSine"=>"easeInSine",
         "easeOutSine"=>"easeOutSine",
         "easeInOutSine"=>"easeInOutSine",
         "easeInQuad"=>"easeInQuad",
         "easeOutQuad"=>"easeOutQuad",
         "easeInOutQuad"=>"easeInOutQuad",
         "easeInCubic"=>"easeInCubic",
         "easeOutCubic"=>"easeOutCubic",
         "easeInOutCubic"=>"easeInOutCubic",
         "easeInQuart"=>"easeInQuart",
         "easeOutQuart"=>"easeOutQuart",
         "easeInOutQuart"=>"easeInOutQuart",
         "easeInQuint"=>"easeInQuint",
         "easeOutQuint"=>"easeOutQuint",
         "easeInOutQuint"=>"easeInOutQuint",
         "easeInExpo"=>"easeInExpo",
         "easeOutExpo"=>"easeOutExpo",
         "easeInOutExpo"=>"easeInOutExpo",
         "easeInCirc"=>"easeInCirc",
         "easeOutCirc"=>"easeOutCirc",
         "easeInOutCirc"=>"easeInOutCirc",
         "easeInBack"=>"easeInBack",
         "easeOutBack"=>"easeOutBack",
         "easeInOutBack"=>"easeInOutBack",
         "easeInElastic"=>"easeInElastic",
         "easeOutElastic"=>"easeOutElastic",
         "easeInOutElastic"=>"easeInOutElastic",
         "easeInBounce"=>"easeInBounce",
         "easeOutBounce"=>"easeOutBounce",
         "easeInOutBounce"=>"easeInOutBounce"
    ),
    'priority'    => 10,
) );
Kirki::add_field( 'visgroup_theme', array(
    'type'        => 'number',
    'settings'    => 'slider_content_anim_speed',
    'label'       => esc_attr__( 'Slider Content Animation Speed (in miliseconds)', 'visgroup' ),
    'section'     => 'slider_sec',
    'default'     => 1200,
    'choices'     => array(
        'min'  => 100,
        'max'  => 10000,
        'step' => 100,
    ),
) );

Kirki::add_field( 'visgroup_theme', array(
    'type'        => 'number',
    'settings'    => 'slider_anim_speed',
    'label'       => esc_attr__( 'Slider Animation Speed (in miliseconds)', 'visgroup' ),
    'section'     => 'slider_sec',
    'default'     => 800,
    'choices'     => array(
        'min'  => 100,
        'max'  => 10000,
        'step' => 100,
    ),
) );

Kirki::add_field( 'visgroup_theme', array(
    'type'        => 'number',
    'settings'    => 'slider_interval',
    'label'       => esc_attr__( 'Slider Interval (in miliseconds)', 'visgroup' ),
    'section'     => 'slider_sec',
    'default'     => 4000,
    'choices'     => array(
        'min'  => 1000,
        'max'  => 10000,
        'step' => 500,
    ),
) );

Kirki::add_field( 'visgroup_theme', array(
    'type'        => 'custom',
    'settings'    => 'slider_typo',
    'label'       => 'Slider Typography',
    'section'     => 'slider_sec',
    'default'     => '<div style="padding: 30px;background-color: #333; color: #fff; border-radius: 50px;">' . esc_html__( 'Goto Typography section to customize Slider typography', 'visgroup' ) . '</div>',
    'priority'    => 10,
) );

Kirki::add_section('general_sec', array(
    'title'       => __('General Options', 'visgroup'),
    'description' => __('Here you can change basic settings of your site', 'visgroup'),
    'panel'       => 'visgroup_theme_option',
    'priority'    => 10,
    'capability'  => 'edit_theme_options',
));

Kirki::add_field('visgroup_theme', array(
    'type'              => 'toggle',
    'settings'          => 'headersticky',
    'label'             => __('Fixed Header', 'visgroup'),
    'description'       => __('Switch between fixed and static header', 'visgroup'),
    'section'           => 'general_sec',
    'default'           => $visgroup_theme_options['headersticky'],
    'priority'          => 10,
    'sanitize_callback' => 'visgroup_sanitize_checkbox',
));
Kirki::add_field('visgroup_theme', array(
    'type'              => 'toggle',
    'settings'          => 'show_top_bar',
    'label'             => __('Show/Hide Topbar', 'visgroup'),
    'section'           => 'general_sec',
    'default'           => $visgroup_theme_options['show_top_bar'],
    'priority'          => 10,
    'sanitize_callback' => 'visgroup_sanitize_checkbox',
));
Kirki::add_field( 'visgroup_theme', array(
    'type'        => 'radio-buttonset',
    'settings'    => 'navigation_style',
    'label'       => __('Navigation Style','visgroup'),
    'section'     => 'general_sec',
    'default'     => 'header-default',
    'priority'    => 10,
	'choices'     => array(
		'header-default'   => esc_attr__( 'Default', 'fortune' ),
		'menu-pills' => esc_attr__( 'Pills', 'fortune' ),
		'menu-colored'  => esc_attr__( 'Colored', 'fortune' ),
	),
) );
Kirki::add_field('fortune_theme', array(
    'type'              => 'custom',
    'settings'          => 'topbarstyle',
    'label'             => __('Topbar Color Styling', 'visgroup'),
    'section'           => 'general_sec',
    'default'           => '<a href="'.admin_url( '/customize.php?autofocus[section]=colors' ).'">' . esc_html__( 'Change Top Bar Color', 'visgroup' ) . '</a>',
    'priority'          => 10,
    'sanitize_callback' => 'visgroup_sanitize_checkbox',
));
Kirki::add_field('visgroup_theme', array(
    'settings'          => 'site_layout',
    'label'             => __('Site Layout', 'visgroup'),
    'description'       => __('Change your site layout to full width or boxed size.', 'visgroup'),
    'section'           => 'general_sec',
    'type'              => 'radio-image',
    'priority'          => 10,
    'transport'         => 'postMessage',
    'default'           => '',
    'sanitize_callback' => 'visgroup_sanitize_text',
    'choices'           => array(
        ''           => get_template_directory_uri() . '/images/1c.png',
        'boxed' => get_template_directory_uri() . '/images/3cm.png',
    ),

));

//if ( !function_exists( 'wp_get_custom_css' ) ) {
    
	Kirki::add_field('fortune_theme', array(
    'type'                 => 'code',
    'settings'             => 'custom_css',
    'label'                => __('Custom Css', 'fortune'),
    'desc'                => __('Custom Css 12312 31231', 'fortune'),
    'section'              => 'general_sec',
    'default'              => '',
    'priority'             => 10,
    'sanitize_callback'    => 'wp_filter_nohtml_kses',
    'sanitize_js_callback' => 'wp_filter_nohtml_kses',
	'choices'     => array(
		'language' => 'css',
		'theme'    => 'monokai',
		'label'=>'Open Css Editor'
	),
));
//}
Kirki::add_field('fortune_theme', array(
    'type'              => 'color',
    'settings'          => 'show_top_bar',
    'label'             => __('Background Color', 'fortune'),
    'section'           => 'callout_section',
    'default'           => '',
    'priority'          => 10,
    'sanitize_callback' => 'fortune_sanitize_color',
	'output'      => array(
        array(
            'element' => '.section.primary',
			'property'=>'background',
        ),
    ),
	
));

/* Typography */
Kirki::add_section('typography_sec', array(
    'title'       => __('Typography Section', 'visgroup'),
    'description' => __('Here you can change Font Style of your site', 'visgroup'),
    'panel'       => 'visgroup_theme_option',
    'priority'    => 160,
    'capability'  => 'edit_theme_options',
));

Kirki::add_field('visgroup_theme', array(
    'type'        => 'typography',
    'settings'    => 'logo_font',
    'label'       => __('Logo Font Style', 'visgroup'),
    'description' => __('Change logo font family and font style.', 'visgroup'),
    'section'     => 'typography_sec',
    'default'     => array(
        'font-style'  => array('bold', 'italic'),
        'font-family' => 'Open Sans',
		'variant'        => 'regular',
		'font-size'      => '36px',
		'line-height'    => '1em',
		'letter-spacing' => '0',
		'subsets'        => array( 'sans-serif' ),
		'color'          => '#333333',
		'text-transform' => 'uppercase',

    ),
    'priority'    => 10,
    'output'      => array(
        array(
            'element' => '.header .logo h1, .header .logo h2',
        ),
    ),
));

Kirki::add_field('visgroup_theme', array(
    'type'        => 'typography',
    'settings'    => 'logo_tag_font',
    'label'       => __('Logo Tag line Style', 'visgroup'),
    'description' => __('Change logo tag ine font family and font style.', 'visgroup'),
    'section'     => 'typography_sec',
    'default'     => array(
        'font-style'  => array('bold', 'italic'),
        'font-family' => 'Open Sans',
		'font-size'      => '11px',
		'line-height'    => '1.5em',
		'subsets'        => array( 'sans-serif' ),
		'color'          => '#a3a3a3',
		'text-transform' => 'uppercase',

    ),
    'priority'    => 10,
    'output'      => array(
        array(
            'element' => '.header .logo .tagline',
        ),
    ),
));

Kirki::add_field('visgroup_theme', array(
    'type'        => 'typography',
    'settings'    => 'prime_menu_font',
    'label'       => __('Primary Menu Style', 'visgroup'),
    'section'     => 'typography_sec',
    'default'     => array(
        'font-style'  => array('bold', 'italic'),
        'font-family' => 'Open Sans',
		'font-size'      => '16px',
/* [SB]    
		'line-height'    => '40px',
*/    
		'subsets'        => array( 'sans-serif' ),
		'text-transform' => 'uppercase',

    ),
    'priority'    => 10,
    'output'      => array(
        array(
            'element' => '.fhmm .navbar-collapse .navbar-nav > li > a',
        ),
    ),
));
/* Slider title Typography */
Kirki::add_field('visgroup_theme', array(
    'type'        => 'typography',
    'settings'    => 'slider_title_font',
    'label'       => __('Slider Title Style', 'visgroup'),
    'description' => __('Change Slider Title font family and font style.', 'visgroup'),
    'section'     => 'typography_sec',
    'default'     => array(
        'font-style'  => array('bold', 'italic'),
        'font-family' => "Open Sans",

    ),
    'default'     => array(
        'font-style'  => array('bold', 'italic'),
        'font-family' => 'Playfair Display',
        'font-size'      => '40px',
        'line-height'    => '50px',
        'subsets'        => array( 'serif' ),
        'text-transform' => 'uppercase',
        'color'          => '#dc2a0b',

    ),
    'priority'    => 10,
    'output'      => array(
        array(
            'element' => '.ei-title h2',
        ),
    ),
));

/* Slider subtitle Typography */
Kirki::add_field('visgroup_theme', array(
    'type'        => 'typography',
    'settings'    => 'slider_subtitle_font',
    'label'       => __('Slider Subtitle Font Style', 'visgroup'),
    'description' => __('Change Sldier subtitle font family and font style.', 'visgroup'),
    'section'     => 'typography_sec',
    'default'     => array(
        'font-family' => 'Open Sans Condensed',
        'font-size'      => '70px',
        'line-height'    => '70px',
        'subsets'        => array( 'sans-serif' ),
        'text-transform' => 'uppercase',
        'color'=>'#000',

    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'  => true,
        'font-family' => true,
    ),
    'output'      => array(
        array(
            'element' => '.ei-title h3',
        ),
    ),
));

/* Full body typography */
Kirki::add_field('visgroup_theme', array(
    'type'        => 'typography',
    'settings'    => 'site_font',
    'label'       => __('Site Font Style', 'visgroup'),
    'description' => __('Change whole site font family and font style.', 'visgroup'),
    'section'     => 'typography_sec',
    'default'     => array(
        'font-style'  => array('bold', 'italic'),
        'font-family' => "Open Sans",

    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'  => true,
        'font-family' => true,
    ),
    'output'      => array(
        array(
            'element' => 'body, h1, h2, h3, h4, h5, h6, p, em, blockquote, .main_title h2',
        ),
    ),
));

/* Home Page Customizer */
Kirki::add_section('home_customize_section', array(
    'title'      => __('Home Page Reorder Sections', 'visgroup'),
    'panel'      => 'visgroup_theme_option',
    'priority'   => 160,
    'capability' => 'edit_theme_options',
));
Kirki::add_field( 'visgroup_theme', array(
	'type'        => 'sortable',
	'settings'    => 'home_sections',
	'label'       => __( 'Here You can reorder your homepage section', 'visgroup' ),
	'section'     => 'home_customize_section',
	'default'     => array(
		'service',
		'portfolio',
		'blog',
		'callout'
	),
	'choices'     => array(
		'service' => esc_attr__( 'Service Section', 'visgroup' ),
		'portfolio' => esc_attr__( 'Portfolio Section', 'visgroup' ),
		'blog' => esc_attr__( 'Blog Section', 'visgroup' ),
		'callout' => esc_attr__( 'Callout Section', 'visgroup' ),
	),
	'priority'    => 10,
) );
/* footer options */
Kirki::add_section('footer_section', array(
    'title'      => __('Footer Options', 'visgroup'),
    'panel'      => 'visgroup_theme_option',
    'priority'   => 160,
    'capability' => 'edit_theme_options',
));
Kirki::add_field('visgroup_theme', array(
    'settings'          => 'footer_bg_color',
    'label'             => __('Footer-1 Background Color', 'visgroup'),
    'section'           => 'footer_section',
    'type'              => 'color-alpha',
    'default'           => '#dcd5cf',
    'priority'          => 10,
    'output'            => array(
        array(
            'element'  => '.footer',
            'property' => 'background',
        ),
    ),
    'transport'         => 'auto',
    'sanitize_callback' => 'visgroup_sanitize_color',
));

Kirki::add_field('visgroup_theme', array(
    'settings'          => 'footer_2_bg_color',
    'label'             => __('Footer-2 Background Color', 'visgroup'),
    'section'           => 'footer_section',
    'type'              => 'color-alpha',
    'default'           => '#cf3c3a',
    'priority'          => 10,
    'output'            => array(
        array(
            'element'  => '.footer-copyright',
            'property' => 'border-top',
        ),
		array(
            'element'  => '.footer-copyright',
            'property' => 'background',
        ),
    ),
    'transport'         => 'auto',
    'sanitize_callback' => 'visgroup_sanitize_color',
)); 

Kirki::add_field('visgroup_theme', array(
    'settings'          => 'footer_layout',
    'label'             => __('Footer Widget Layout', 'visgroup'),
    'description'       => __('Change footer widget area into 2, 3 or 4 column', 'visgroup'),
    'section'           => 'footer_section',
    'type'              => 'radio-image',
    'priority'          => 10,
    'default'           => $visgroup_theme_options['footer_layout'],
    'transport'         => 'postMessage',
    'choices'           => array(
        2 => get_template_directory_uri() . '/images/footer-widgets-2.png',
        3 => get_template_directory_uri() . '/images/footer-widgets-3.png',
        4 => get_template_directory_uri() . '/images/footer-widgets-4.png',
    ),
    'sanitize_callback' => 'visgroup_sanitize_number',
));
Kirki::add_field('visgroup_theme', array(
    'type'              => 'toggle',
    'settings'          => 'show_footer_widget',
    'label'             => __('Show/Hide Footer Widget Area', 'visgroup'),
    'section'           => 'footer_section',
    'default'           => $visgroup_theme_options['show_footer_widget'],
    'priority'          => 10,
    'sanitize_callback' => 'visgroup_sanitize_checkbox',
));
?>