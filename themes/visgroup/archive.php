<?php get_header(); ?>
<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <?php if (have_posts()) { ?>
                <div class="col-md-6">
                    <h1><?php the_archive_title(); ?></h1>
                </div><?php }
            ?>
            <div class="col-md-6">
                <?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Heading / End -->
<!-- Page Content -->
<section class="page-content">
    <div class="container">

        <div class="row">
            <div class="content col-md-8"><?php
                if (have_posts()) {
                    while (have_posts()): the_post();
                        get_template_part('blog', 'content');
                    endwhile;
                }
                wp_link_pages();
                visgroup_pagination();
                ?>
            </div>
            <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">

                    <h3 class="widget-title">Categories</h3>
                    <ul id="scroll_links">
                        <?php
                       $categories = get_categories( array(
                            'orderby' => 'name',
                            'order'   => 'ASC'
                        ) );

                        foreach( $categories as $category ) {
                            $category_link = sprintf( 
                                '<a href="%1$s" alt="%2$s">%3$s</a>',
                                esc_url( get_category_link( $category->term_id ) ),
                                esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
                                esc_html( $category->name )
                            );

                            echo '<li>' . sprintf( esc_html__( '%s', 'textdomain' ), $category_link ) . '</li> ';
                            
                        } 
                        ?>
                    </ul>
                </div>


            </aside>
        </div>
    </div>
</section>
<?php get_footer(); ?>
