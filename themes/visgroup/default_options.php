<?php
/* General Options */
function visgroup_theme_options()
{ 
    $visgroup_theme_options = array(
        'site_layout' => '',
        'upload_image_logo' => '',
        'headercolorscheme' => 'light_header',
        'headersticky' => 1,
        'custom_css' => '',
		'home_service_enabled' => 1,
        'service_type'=>1,
        'show_top_bar'=>0,
		'color_scheme'=>'',
		'navigation_style'=>'header-default',
		'service_heading'=>__('Our Services','visgroup'),
        'service_title_1' => __("Responsive", 'visgroup'),
        'service_icon_1' => "fa fa-mobile",
        'service_text_1' => __("Lorem ipsum dolor sit amet, consectetur adipisicing elit ipsum lorem sit amet.", 'visgroup'),
        'service_link_1' => "#",
		'service_target_1' =>'' ,

        'service_title_2' => __("Retina Ready", 'visgroup'),
        'service_icon_2' => "fa fa-eye",
        'service_text_2' => __("Lorem ipsum dolor sit amet, consectetur adipisicing elit ipsum lorem sit amet.", 'visgroup'),
        'service_link_2' => "#",
		'service_target_2'=>'',

        'service_title_3' => __("Multi Layout", 'visgroup'),
        'service_icon_3' => "fa fa-copy",
        'service_text_3' => __("Lorem ipsum dolor sit amet, consectetur adipisicing elit ipsum lorem sit amet", 'visgroup'),
        'service_link_3' => "#",
		'service_target_3'=>'',
		'slider_content_anim_speed'=>5200,
        'slider_anim_speed'=>800,
		//Slider Settings:
        'slider_home' => 1,
		'slider_category'=>'',
        'slider_interval'=>4000,
        'slider_easing_effect'=>'easeOutExpo',
        'slider_auto_play'=>1,
        //Portfolio Settings:
        'portfolio_home' => 0,
        'portfolio_post' => "",
        'footer_copyright' => __('VisGroup Theme', 'visgroup'),
        'developed_by_text' => __('developed by', 'visgroup'),
        'developed_by_link_text' => __('UiB VisGroup', 'visgroup'),
        'developed_by_link' => 'http://www.ii.uib.no/vis/',
        'footer_layout' => 4,
        'show_footer_widget'=>0,
		'blog_home'=>1,
        'blog_title' => __('Recent Posts', 'visgroup'),
		'blog_desc' => __('There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour of this randomised words which don\'t look even slightly believable', 'visgroup'),
		'home_post_cat' => '',
        /* footer callout */
        'callout_home' => 1,
        'callout_title' => __('Best Wordpress Resposnive Theme Ever!', 'visgroup'),
        'callout_btn_text' => __('Download Now', 'visgroup'),
        'callout_btn_link' => 'http://www.example.com',
        /* Social media icons */
        'contact_info_header' => 1,
        'social_footer' => 1,
		'contact_in_header'=>1,
        'contact_phone' => '+0744-9999',
        'contact_email' => 'example@gmail.com',
		'social_home'=>1,
        'social_facebook_link' => '#',
        'social_twitter_link' => '#',
        'social_instagram_link' => '#',
        'social_linkedin_link' => '#',
        'social_youtube_link' => '#',
        'social_vimeo_link' => '#',
        'social_google_plus_link' => '#',
        'social_skype_link' => '#',
		'home_sections'=>array('service', 'portfolio', 'blog', 'callout')

    );
    return wp_parse_args(get_option('visgroup_theme_options', array()), $visgroup_theme_options);
}

?>