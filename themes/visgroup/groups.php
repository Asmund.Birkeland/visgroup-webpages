<?php
/**
 * Template Name: Groups
 */
get_header();
the_post();

// Get 'team' posts
$groups_posts  = get_posts(array(
    'post_type' => 'groups',
    'posts_per_page' => -1, // Unlimited posts
    'orderby' => 'title', // Order alphabetically by name
    'order' => 'ASC',
        ));
?>

<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-md-6">
                <?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Content -->
<section class="page-content">
    <div class="container">
        <div class="row">
        
        
            <div class="content col-md-8">
                <div class="content col-md-12">
                    
                    <?php
                    if (has_post_thumbnail())
                    {
                        $img_class = array('class' => 'img_responsive');
                        ?>
                        <figure class="alignleft"> <?php the_post_thumbnail('visgroup_page_thumb', $img_class); ?> </figure>
                        <?php 
                    }
                    the_content();
                       
                    foreach ($groups_posts  as $post):
                        present_post($post, false);


                     endforeach; 
                    ?>

                </div>

            </div>    
            <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                   <hr class="visible-sm visible-xs lg">
                    <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                        
                        <h3 class="widget-title"></h3>		
                        <ul id="scroll_links">
                            <?php
                                                        foreach ($groups_posts as $group  ):                                
                                echo "<li><a href=". get_permalink($group->ID) . ">" . get_the_title($group->ID) . "</a></li>";
                            endforeach;
                            ?>
                        </ul>
                    </div>
                
                
            </aside>
        </div>
    </div>
</section>
<!-- Page Content / End -->
<?php get_footer();     

function present_post($post, $skipongoing)
{
    setup_postdata($post);
    $thumb_src = null;
    if (has_post_thumbnail($post->ID)) {
        $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'team-thumb');
        $thumb_src = $src[0];
    }
    
    $head = get_field('group_head');
    
    ?>  

    <div class="row team-entry"  >
        <div class="team-image" >
            <?php if (has_post_thumbnail()) :
                echo get_the_post_thumbnail($post->ID,'thumbnail');
              elseif ($head and has_post_thumbnail($head->ID)):
                $src = wp_get_attachment_image_src(get_post_thumbnail_id($head->ID), 'team-thumb');
                $thumb_src = $src[0];  ?>                
                 <img  src="<?php echo $thumb_src ?>" alt="IMG"/>
                <?php 
                //echo get_the_post_thumbnail($head->ID,'thumbnail');
              else: ?>
                <img  src= "<?php bloginfo('template_directory'); ?>/images/noimage.png"/>
            <?php endif; ?>
        </div>
        <div class="team-info">

            <h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
            <h4><?php the_field('group_topics'); ?></h4>
            <p>
            <div class="team-details">
                <ul class="fa-ul">         
                  <?php if ($head): ?>
                    <li><i class="fa fa-user fa-li"></i><a href="<?php echo the_permalink(); ?>" title="<?php echo get_the_title($head->ID); ?>"><?php echo get_field("team_position",$head->ID) . " " . get_the_title($head->ID); ?></a></li>
                    
                    <?php if ($email = get_field('team_email',$head->ID)): ?>
                        <li><i class="fa fa-envelope fa-li"></i><a href="mailto:<?php echo antispambot($email); ?>"><?php echo antispambot($email); ?></a></li>
                    <?php endif; ?>

                    <?php if ($phone = get_field('team_phone',$head->ID)): ?>
                        <li><i class="fa fa-phone fa-li"></i><a href="tel:+47 555 84 <?php echo $phone; ?>">+47 555 84 <?php echo $phone; ?></a></li>
                    <?php endif; ?>                    
                    
                  <?php endif; ?>                             
                </ul>
            </div>
            </p>
        </div>

    </div>
<?php
    wp_reset_postdata(); 
}
?>