<?php get_header(); ?>
<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-md-6">
                <?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<section class="page-content">
    <div class="container">

        <div class="row">
            <div class="content col-md-8">
                <?php $class = get_post_format() == "" ? 'standard' : get_post_format(); ?>
                <?php if (have_posts()) : 
                    while (have_posts()) : the_post(); ?>
                        <!-- Post (Standard Format) -->
                        <article id="post-<?php the_ID(); ?>" <?php post_class('entry entry__' . $class . ' entry__without-icon'); ?>>
<!--                            <div class="entry-icon visible-md visible-lg"><?php
                                $icon = get_post_format() == 'quote' ? 'quote-left' : get_post_format();
                                $icon = $icon == "video" ? 'film' : $icon;
                                $icon = $icon == "" ? 'file' : $icon;
                                ?>
                                <i class="fa fa-<?php echo $icon; ?>"></i>
                            </div>-->
                            <header class="entry-header">
                                <h2><?php the_title(); ?></h2>
                                <div class="entry-meta">
                                    <span class="entry-date">
                                        <?php echo get_the_date(get_option('date_format'), get_the_ID()); ?>
                                    </span>
                                    
                                    <span class="entry-category"><?php _e('in ', 'visgroup');
                                        echo get_the_category_list(','); ?>
                                    </span>
                                    
                                </div>
                            </header><?php
                            if (has_post_thumbnail()) {
                                $post_image_id = get_post_thumbnail_id();
                                $post_image = wp_get_attachment_image_src($post_image_id, 'full');
                                ?>
                                <figure class="alignnone post-thumb entry-thumb">
                                    <a href="<?php echo esc_url($post_image[0]); ?>" class="fancybox">
                                        <?php the_post_thumbnail('visgroup_post_single'); ?>
                                    </a>
                                </figure><?php }
                            ?>
                            <div class="entry-content">
                                <?php the_content();
                                wp_link_pages();
                                ?>
                            </div>
                            
                        </article>
                        <!-- Post (Standard Format) / End -->

                        <!-- Comments -->
                <?php endwhile;
            endif;
            ?>
                <!-- Comments / End -->

            </div>
            
             <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                    <?php
                    $category = get_the_category(get_the_ID());
                        $title = 'Projects';
                        $options = array(
                            'post_type' => 'projects',
                            'posts_per_page' => -1, // Unlimited posts
                            'orderby' => 'title', // Order alphabetically by name
                            'order' => 'ASC'
                        );
                        if ($category[0]->name == 'News'):
                           $title = 'News';
                           $options =  array(
                            'post_type' => 'post',
                            'category_name' => 'News',
                            'posts_per_page' => -1, // Unlimited posts
                            'orderby' => 'title', // Order alphabetically by name
                            'order' => 'ASC'
                            );
                        endif;
                        ?>
                    <h3 class="widget-title"><?php echo $title; ?></h3>
                    <ul id="scroll_links">
                        <?php
                        
                        $job_posts = get_posts($options);
                        foreach ($job_posts as $jobs):
                            
                            echo "<li><a href=" . get_permalink($jobs->ID) . ">" . $jobs->post_title . "</a></li>";
                        endforeach;
                        ?>
                    </ul>
                </div>


            </aside>
            
        </div>
    </div>
</section>
<?php get_footer(); ?>