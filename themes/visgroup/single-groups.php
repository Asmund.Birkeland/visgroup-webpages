<?php
get_header();
the_post();

// Get members posts
$head = get_field("group_head");
$members = get_field("group_members");


?>

<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-md-6">
                <?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Content -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="content col-md-8">
                <div class="content col-md-12" >
                    <?php
                    if (has_post_thumbnail()) {
                        $img_class = array('class' => 'img_responsive');
                        ?>
                        <figure class="alignleft"> <?php the_post_thumbnail('visgroup_page_thumb', $img_class); ?> </figure>
                    <?php } ?>
                    <?php the_content(); ?>

                    <?php


                    if ($members):
                      function title_rank($str, $name) {
                          $lower = strtolower($str);
                          $is_adjunct = stripos($lower, 'adju') !== false;
                          $is_associate = stripos($lower, 'assoc') !== false;
                          $is_professor = stripos($lower, 'prof') !== false;
                          $is_post = stripos($lower, 'post') !== false;
                          $is_student = stripos($lower, 'student') !== false;
                          $is_phd = stripos($lower, 'phd') !== false;

                          if ($is_adjunct and $is_associate and $is_professor) { return 3; }
                          if ($is_associate and $is_professor) { return 1; }
                          if ($is_adjunct and $is_professor) { return 2; }
                          if ($is_professor) { return 0; }
                          if ($is_post) { return 4; }
                          if ($is_student and $is_phd) { return 5; }

                          return 6;
                      }

                      $compare_fn = function($a, $b) {
                          setup_postdata($a);
                          $nameA = explode(" ", get_the_title($a));
                          $lastNameA= $nameA[count($nameA)-1];
                          $positionA = get_field('team_position', $a);
                          $rankA = title_rank($positionA, $lastNameA);

                          setup_postdata($b);
                          $nameB = explode(" ", get_the_title($b));
                          $lastNameB = $nameB[count($nameB)-1];
                          $positionB = get_field('team_position', $b);
                          $rankB = title_rank($positionB, $lastNameB);

                          // TODO, if alumni, use - ( time TO ) to compare!

                          return strnatcasecmp($rankA . $lastNameA, $rankB . $lastNameB);
                      };
                      
                      usort($members, $compare_fn);                      

                      $post = $head;
                      present_member(false);
                      
                      foreach ($members as $post):
                        present_member(true);
                      endforeach; 
                      
                      wp_reset_postdata();
                    endif;
                  ?>
                                    
                </div>
            </div>
            
            <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                    <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">
                        
                        <h3 class="widget-title">Job Offers</h3>		
                        <ul id="scroll_links">
                            <?php
                            
                            $job_posts = get_posts(array(
                               'post_type' => 'post',
                               'category_name' => 'jobs',
                                'posts_per_page' => -1, // Unlimited posts
                                'orderby' => 'title', // Order alphabetically by name
                                'order'=>'ASC'
                                ));
                            foreach ($job_posts as $jobs  ):
                                
                                echo "<li><a href=". get_permalink($jobs->ID) . ">" . $jobs->post_title."</a></li>";
                            endforeach;
                            ?>
                        </ul>
                    </div>
                

            </aside>
        </div>
    </div>

</section>
<!-- Page Content / End -->
<?php get_footer(); 

function present_member($skiphead = true)
{
  global $head;
  global $post;
  
  setup_postdata($post);

  if ($skiphead) {  
    if ($post->ID == $head->ID) {
      return;
    }
  }

  if (get_field('team_alumni',$post->ID)) {
    return;
  }    

  // Resize and CDNize thumbnails using Automattic Photon service
  $thumb_src = null;
  if (has_post_thumbnail($post->ID)) {
      $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'team-thumb');
      $thumb_src = $src[0];
  }
  ?>
  <div class="content col-md-6 col-xs-12">
      <div class="team-entry">
          <div class="team-image">
              <?php if (has_post_thumbnail($post->ID)) : ?>
                  <img  src=<?php echo $thumb_src?> alt="IMG"/>
              <?php else: ?>
                  <img  src= "<?php bloginfo('template_directory');?>/images/noimage.png"/>
              <?php endif; ?>
          </div>

          <div class="team-info ">

              <h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
              <h4><?php the_field('team_position',$post->ID); ?></h4>
              <p><?php the_field('team_topics',$post->ID); ?></p>
              <!-- <?php the_content(); ?> -->

              <div class="team-details">
                  <ul class="fa-ul">         
                      <?php if ($email = get_field('team_email',$post->ID)): ?>
                          <li><i class="fa fa-envelope fa-li"></i><a href="mailto:<?php echo antispambot($email); ?>"><?php echo antispambot($email); ?></a></li>
                      <?php endif; ?>

                      <?php if ($phone = get_field('team_phone',$post->ID)): ?>
                          <li><i class="fa fa-phone fa-li"></i><a href="tel:+47 555 84 <?php echo $phone; ?>">+47 555 84 <?php echo $phone; ?></a></li>
                      <?php endif; ?>

                      <?php if ($office = get_field('team_office',$post->ID)): ?>
                          <li><i class="fa fa-home fa-li"></i>
                              <?php if ($mazemapPOI = get_field('team_mazemap_poi',$post->ID)): ?>
                                  <a href="https://use.mazemap.com/?v=1&sharepoitype=poi&campusid=34&left=5.3311&right=5.3321&top=60.3814&bottom=60.3811&sharepoi=<?php echo $mazemapPOI; ?>">HiB, room <?php echo $office; ?></a>
                              <?php else: ?>
                                  HiB, room <?php echo $office; ?>
                              <?php endif; ?>
                          </li>
                      <?php endif; ?>
                  </ul>
              </div>

              <?php if ($twitter = get_field('team_twitter',$post->ID)): ?>
                  <a href="<?php echo $twitter; ?>"><i class="icon-twitter"></i></a>
              <?php endif; ?>
              <?php if ($linkedin = get_field('team_linkedin',$post->ID)): ?>
                  <a href="<?php echo $linkedin; ?>"><i class="icon-linkedin"></i></a>
              <?php endif; ?>
          </div>
      </div>
  </div>
<?php   
}
?>
