<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<script type="text/javascript">
    jQuery(document).ready(function(){
      jQuery('a[href="#search"]').on('click', function(event) {                    
        jQuery('#search').addClass('open');
        jQuery('#search > form > input[type="search"]').focus();
      });            
      jQuery('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.id == 'close' || event.keyCode == 27) {
          jQuery(this).removeClass('open');
        }
      });            
    });
	</script>
</head>
<?php 
$visgroup_theme_options = visgroup_theme_options();
global $logo;
$args = array(
        'post_type' => 'attachment',
        'post_mime_type' =>'image',
        'post_status' => 'inherit',
        'posts_per_page' => -1
        
       
    );
    $query_images = get_posts( $args );
    $images = array();
    foreach ( $query_images as $image) {
        #echo get_the_title($image);
        if (get_the_title($image) == "GroupLogo"){
            $logo= $image->guid;
        }
}

?>
<body <?php body_class($visgroup_theme_options['site_layout']); ?>>

  <!-- Search Form -->
  <div id="search"> 
    <i id="close" class="fa fa-times close"></i>
    <form role="search" id="searchform" action="<?php echo esc_url(home_url('/')); ?>" method="get" role="search">
      <input type="search" value="" name="s" id="s" placeholder="<?php esc_attr_e('Search here...','visgroup'); ?>"/>
    </form>
  </div>

	<div class="site-wrapper">
		
		<!-- UiB Header -->
    <div class="uib">
      <div class="uib-header-wrapper">
        <header class="uib-header">
          <a href="http://www.uib.no/en" title="Home" rel="home" class="uib-logo-link">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/uib-logo.svg" alt="Home" class="uib-logo-image">
          </a>
          <div class="uib-site-name">
            <a href="http://www.uib.no/en" title="University of Bergen"><span>University of Bergen</span></a>
            <span class="uib-bullet">&bull;</span>
          </div>
          <div class="uib-department-name">
            <a href="http://www.uib.no/en/ii" title="Department of Informatics"><span>Department of Informatics</span></a>
          </div>
          
          <div class="uib-region-header">         
          </div>
          
          <!-- Search Link -->
          <div class="uib-search">
            <a href="#search">
              <i class="searchbutton"></i>
            </a>
          </div>          

                    
<!--
          <div id="block-uib-search-global-searchform" class="block block-uib-search">
            
            <div class="uib-content">
              <form role="search" name="noscriptform" action="/" method="get" id="uib-search-noscript-form" accept-charset="UTF-8">
                <div>
                  <input class="search-button" type="submit" value="Search">
                </div>
              </form>              
            </div>          
        </div>
-->          
      </div>
    </div>  

		<!-- Header -->
		<header class="header <?php echo $visgroup_theme_options['navigation_style']?>">

		<?php	if($visgroup_theme_options['show_top_bar']){ ?>
		<div class="header-top">
			<div class="container">
			<?php if ( has_nav_menu( 'secondary' ) ) : ?>
				<div class="header-top-left">
					<?php wp_nav_menu(array(
						'theme_location' => 'secondary',
						'container' => false,
						'menu_class' => 'header-top-nav',
						)
					); ?>
				</div><?php endif;?>
				<?php
					if($visgroup_theme_options['contact_in_header']){?>
					<div class="header-top-right">
						<span class="login">
							<i class="fa fa-phone"></i> <?php _e('Call US:','visgroup');?> <a href="tel:<?php echo esc_attr($visgroup_theme_options['contact_phone']);?>"><?php echo esc_attr($visgroup_theme_options['contact_phone']);?></a>
						</span>
						<span class="register">
						<i class="fa fa-envelope-o"></i>	<?php _e('Email:','visgroup');?> <a href="mailto:<?php echo sanitize_email($visgroup_theme_options['contact_email']);?>"><?php echo sanitize_email($visgroup_theme_options['contact_email']);?></a>
						</span>
					</div><?php
					} ?>
			</div>
		</div><?php } ?>
			<div class="header-main" style="background-image:url(<?php echo esc_url(get_header_image()); ?>);">
				<div class="container">
					<nav class="navbar navbar-default fhmm" role="navigation">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle">
								<i class="fa fa-bars"></i>
							</button>
							<!-- Logo -->
							<div class="logo">
									<a href="<?php echo esc_url(home_url('/')); ?>" class="visgroup-logo-link" itemprop="url">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/visgroup-logo.svg" class="visgroup-logo" alt="<?php echo get_bloginfo('name'); ?>" itemprop="logo">
									</a>
							</div>
							<!-- Logo / End -->
						</div><!-- end navbar-header -->

						<div id="main-nav" class="navbar-collapse collapse" role="navigation">
						<!-- Menu Goes Here -->
						<?php wp_nav_menu(array(
								'theme_location' => 'primary',
								'container' => false,
								'menu_class' => 'nav navbar-nav',
								'fallback_cb' => 'visgroup_fallback_page_menu',
								'walker' => new visgroup_nav_walker(),
								)
							); ?>
						</div>
					</nav>
				</div>
			</div>
				
		</header>
		<!-- Header / End -->
		<!-- Main -->
		<div class="main" role="main">
