<?php
/**
 * Template Name: events
 */
get_header();
the_post();


$currentDate =date('Y-m-d');
$today = explode("/", date('Y-m-d'));
// Get 'team' upcoming posts
$event_posts_asc = query_posts( array(
  'post_type' => 'events',
  'order' => 'ASC',
  'orderby' => 'meta_value',
  'meta_key' => 'event_date',
  'posts_per_page' => -1, // Unlimited posts
  'meta_compare' => '>=',
  'meta_value' => date('Y-m-d'),

) );
// Past posts
$event_posts_desc = query_posts (array(
  'post_type' => 'events',
  'order' => 'DESC',
  'orderby' => 'meta_value',
  'meta_key' => 'event_date',
  'posts_per_page' => -1, // Unlimited posts
  'meta_compare' => '<',
  'meta_value' => date('Y-m-d'),
) );
$categories = array();
foreach ($event_posts_desc as $post):
    $custom_fields = get_post_custom($post->ID);
    $cat_field = $custom_fields['event_category'];
    if($cat_field[0]){
        array_push($categories, $cat_field[0]);
    }

endforeach;
$categories = array_unique($categories);
global $categories;
global $event_posts_asc;
global $event_posts_desc;
?>


<style>

    tr { display: block; float: left; }
    th, td { display: block; }
</style>

<!-- Page Heading -->
<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Events</h1>
            </div>
            <div class="col-md-6">
                <?php visgroup_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>
<!-- Page Content -->
<section class="page-content">
    <div class="container">
        <div class="row">

             <div class="col-md-8">

                <div class="tab-content">
                <?php
                createTab("Upcoming events","All");
                foreach ($categories as $cat ):

                    createTab($cat."s",$cat);
                endforeach;

                //createTab('All','');
                //createTab('VCF','VCF');
                //createTab('Workshops','Workshop');
                //createTab('Conferences', 'Conference');
                //createTab('Seminars', 'Seminar');
                ?>
                </div>

            </div>
             <aside class="sidebar col-md-3 col-md-offset-1 col-bordered" >
                <hr class="visible-sm visible-xs lg">
                    <div class="widget_categories widget widget__sidebar" id="custom_sidebar" style="">

                        <h3 class="widget-title">Categories</h3>

                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a data-toggle="tab" href="#All">All</a></li>

                            <?php
                            foreach ($categories as $cat ):
                                echo "<li><a data-toggle='tab' href='#" . preg_replace('/\s+/', '_', $cat) ."'>".$cat."s</a></li>";
                            endforeach;
                                ?>



                          </ul>




                    </div>


            </aside>
        </div>
    </div>
</section>


<!-- Page Content / End -->
<?php get_footer();

 function createTab($title,$category){
     global $event_posts_asc;
     global $event_posts_desc;
     global $post;

     $category = preg_replace('/\s+/', '_', $category);
     $active = '';
     $events = $title;
     if ($category == 'All'):
         $active = 'active';
         $events = 'Events';
     endif;


    ?>

        <div id="<?php echo $category?>" class="tab-pane fade in <?php echo $active?>">
            <h1><?php echo $title;?></h1>
            <?php
            foreach ($event_posts_asc as $post):
                $cat = get_post_field('event_category',$post->ID);
                $cat = preg_replace('/\s+/', '_',$cat);
                if ($cat == $category or $category == 'All'):
                    presentPost($post, false);
                endif;

            endforeach;
             echo "<h1>Past ". $events . "</h1>";
            foreach ($event_posts_desc as $post):
                $cat = get_post_field('event_category',$post->ID);
                $cat = preg_replace('/\s+/', '_',$cat);
                if ($cat == $category or $category == 'All'):

                    presentPost($post, true);
                endif;

            endforeach;
            ?>
        </div>

<?php

 }

function presentPost($post, $pastEvents)
{
    global $post;

    setup_postdata($post);

     // Resize and CDNize thumbnails using Automattic Photon service
    $thumb_src = null;
    if (has_post_thumbnail($post->ID)) {
        $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'team-thumb');
        $thumb_src = $src[0];
    }
    ?>

    <div class="content col-md-12">

        <div class="row team-entry">
            <div class="team-image" >
                <?php if (has_post_thumbnail()) { ?>
<!--                    <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                        <img  src=<?php echo $thumb_src ?> </img>
                    </a>-->
                        <?php echo get_the_post_thumbnail($post->ID,'thumbnail');?>
                <?php } ?>
            </div>

            <div class="team-info">
                 <h3>
                    <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                    <?php the_title(); ?>
                    </a>
                </h3>
                <?php
                $speaker = get_field('event_speaker');
                if ($speaker)
                {
                    echo $speaker;
                    echo "<br>";
                }
                $locationDesc = get_field('event_location_description');

                if ($locationDesc)
                {
                    echo $locationDesc;
                    echo "<br>";
                }

                $date = get_field('event_date');
                if ($date)
                {
                    echo $date;
                    echo "<br>";
                }
                $time = get_field('event_time');
                if ($time)
                {
                    echo $time;
                    echo "<br>";
                }

                $category = get_field('event_category');
                if ($category)
                {
                    echo $category;
                }
                ?>


            </div>
        </div>
    </div><!-- /.profile -->

<?php
}
?>
